<?php
/*
    Добавление значений к массиву
 */
interface Collection{
    public function pull();
    public function push($array);
    public function reset();
}