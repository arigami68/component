<?php
class DPD extends DPDConnect{
    public static $OPERATION_getStatesByClient       = 'getStatesByClient';
    public static $OPERATION_getStatesByClientParcel = 'getStatesByClientParcel';
    public static $OPERATION_getStatesByDPDOrder     = 'getStatesByDPDOrder';
    public $OPERATION_CONFIRM = 1;
    public $docId;
    public $report = TRUE;
    public function start(TrackComponent $track, $data){
        try{
            if(!isset($data['agent'])) throw new DPDNotTrackException();
            if(!isset($data['method'])) throw new DPDNotTrackException();
            $agent   = $data['agent'];
            $method  = $data['method'];

        }catch(DPDException $dpd){
            die($dpd::$NOT_FIND_START);
        }

        if(true){
            $data               = array();
            $array              = array();
            $track_full         = new TrackFull();
            $track_headcomponent= new TrackHeadComponent();
        }
        switch($method){
            case 'getStatesByClient':
				try{
                	$data =  $this->getStatesByClient();
				}catch(DPDException $exc){
					if($exc->code == 2000){
						die('Нет трекинга');
					}
					print_r($exc);
					exit;
				}
            break;
            case 'getStatesByClientParcel':
                $this->OPERATION_CONFIRM = 0;
                if(!$data = $this->getStatesByClientParcel($agent)){
                    $method = self::$OPERATION_getStatesByDPDOrder;
                    $data = $this->getStatesByDPDOrder($agent);
                }
            break;
        };

       # try{$data = $this->$getStatesByClient();}catch(DPDException $dpd){ die($dpd::$REQUEST_NOT_ANSWER);}

        if($data){
            $parsing = new DPDParsing(new StepComponent);

            try{$result = $parsing->$method($data->return);}catch(DPDException $dpd){ die($dpd::$REQUEST_NOT_TRACK);}

            if($result){

                foreach($result as $key=>$value){

                    $full      = clone $track_full;
                    $agent = "$key";
                    if($order = ORDS::model()->megapolis($agent)->find()){
                        $flag = 1; # Это отправление есть в Нашей базе
                        $full->agent = $order->agent;
                        $full->dbOrder = $order;
                        $full->step = $value;
                        if(true){
                            $head = clone $track_headcomponent;
                            $head->dbOrder = $order;
                            $head->np = $order->costDelivery;
                            $head->oc = $order->costPublic;
                            $head->weight = $order->weightEnd;
                            $head->code = $order->agent;
                            $head->address = $order->address;
                            $head->zip = $order->zip;
                        }
                        $full->head = $head;
                        $array[] = $full;
                    }else{
                        $flag = 0; # Этого отправления нет в нашей базе
                    }
                    if($this->report){
                        // Добавляем отчет в таблицу
                        foreach($value as $track_value){
                            $track_pdp              = new DBtrack_dpd();
                            $track_pdp->code        = $agent;
                            $track_pdp->date        = date('Y-m-d H:i:s', $track_value->date);
                            $track_pdp->zip         = empty($track_value->zip) ? '' : $track_value->zip;
                            $track_pdp->address     = $track_value->address;
                            $track_pdp->operation   = '';
                            $track_pdp->status      = $track_value->status;
                            $track_pdp->flag        = $flag;
                            $track_pdp->flagDate    = date('Y-m-d H:i:s');
                            $track_pdp->insert();
                        }
                    }
                }
            }
        }
        return $array;
    }
    public function process(){
        return TRUE;
    }
    public function end(){
        if($this->OPERATION_CONFIRM){
            $result = $this->confirm($this->docId);
        }else{
            $result = null;
        }
        return $result;
    }
}
class DPDConnect{
    private $dpd = array(
        'auth' => array(
            'id' => '1001022000',
            'key' => 'D3ECA2C0C164DE98FA67E1E14F5CBF749501F0B3',
        ),
        'wsdl' => array(
            'getStatesByClientOrder'     => 'http://ws.dpd.ru:80/services/tracing?wsdl',
            'getStatesByClientParcel'    => 'http://ws.dpd.ru:80/services/tracing?wsdl',
            'getStatesByClient'          => 'http://ws.dpd.ru:80/services/tracing?wsdl',
        )
    );
    public function __construct(){

    }
    public function addAuth(){
        $data[ 'auth' ][ 'clientNumber' ] = $this->dpd['auth']['id'];
        $data[ 'auth' ][ 'clientKey' ]    = $this->dpd['auth']['key'];

        return $data;
    }
    public function getStatesByClient(){
		/*
        try{
            $client = new SoapClient( $this->dpd['wsdl']['getStatesByClient']);
        }catch (SoapFault $exc ){
            print 'нет ответа';
            return null;
        }
		*/
		$client = new SoapClient( $this->dpd['wsdl']['getStatesByClient']);
        $data = array( 'request' => $this->addAuth());
        $result = $client->getStatesByClient( $data );

        if(!$this->docId = $result->return->docId) throw new DPDNotTrackException();

        return $result;
    }
    public function getStatesByClientParcel($agent){ # Номер посылки в информационной системе клиента
        $client = new SoapClient( $this->dpd['wsdl']['getStatesByClientParcel']);
        $data = array( 'request' => $this->addAuth());
		$data['request']['clientParcelNr']= $agent;


        try{
            $result = $client->getStatesByClientParcel( $data );
        }catch (SoapFault $exc ){
            return null;
        };

        if(!$this->docId = $result->return->docId) throw new DPDNotTrackException();
        return $result;
    }
    public function getStatesByDPDOrder($agent){ #Номер заказа в информационной системе DPD
        $client = new SoapClient( $this->dpd['wsdl']['getStatesByClientParcel']);

        try{
            $client = new SoapClient( $this->dpd['wsdl']['getStatesByClientParcel']);
        }catch (SoapFault $exc ){
            print 'нет ответа';
            return null;
        }
        $data = array( 'request' => $this->addAuth());
        $data['request']['dpdOrderNr']= $agent;

        try{
            $result = $client->getStatesByDPDOrder( $data );
        }catch (SoapFault $exc ){
            return null;
        }
        if(!$this->docId = $result->return->docId) throw new DPDNotTrackException();
        return $result;
    }
	/*
	 * @name :: confirm
	 * @comment :: Закрытие статусов
	 */
    public function confirm($confirm){
		/*
        $client = new SoapClient( $this->dpd['wsdl']['getStatesByClient']);
        $data = array( 'request' => $this->addAuth());
        $data['request']['docId'] = $confirm;

        return $client->Confirm( $data );
		*/
    }
}
class DPDParsing{
    public $track;
    public function __construct(StepComponent $step){
        $this->track = $step;
    }
    public function getStatesByClient($data){

        if(!isset($data->states)) throw new DPDNotFormatException();
        $array = array();
        $states = (count($data->states) === 1) ? array($data->states) : $data->states;

        foreach($states as $value){
            $array[] = $this->processingONE($value);

            $agent = isset($value->clientOrderNr) ? $value->clientOrderNr : $value->dpdOrderReNr;
            if($ORDS = ORDS::model()->agent($agent)->find()){
                $ORDS->megapolis = $value->dpdOrderNr;
                $ORDS->update();
            }
        }

        foreach($array as $dpdComponent){
            $result[$dpdComponent->dpdOrderNr][] = $this->inTrack($dpdComponent);
        }

        return $result;
    }
    public function getStatesByClientParcel($data){

        if(!isset($data->states)) throw new DPDNotFormatException();
        $array = array();
        $states = (count($data->states) === 1) ? array($data->states) : $data->states;

        foreach($states as $value){
            $array[] = $this->processingONE($value);

            $agent = isset($value->clientOrderNr) ? $value->clientOrderNr : $value->dpdOrderReNr;
            if($ORDS = ORDS::model()->agent($agent)->find()){
                $ORDS->megapolis = $value->dpdOrderNr;
                $ORDS->update();
            }
        }

        foreach($array as $dpdComponent){
            $result[$dpdComponent->dpdOrderNr][] = $this->inTrack($dpdComponent);
        }


        return $result;
    }
    public function getStatesByDPDOrder($data){

        if(!isset($data->states)) throw new DPDNotFormatException();
        $array = array();
        $states = (count($data->states) === 1) ? array($data->states) : $data->states;

        foreach($states as $value){
            $array[] = $this->processingONE($value);

            $agent = $value->dpdOrderNr;
            $megapolis = isset($value->dpdOrderReNr) ? $value->dpdOrderReNr : $value->dpdOrderNr;

            if($ORDS = ORDS::model()->agent($agent)->find()){
                    $ORDS->megapolis = $megapolis;
                    $ORDS->update();
            }
        }

        foreach($array as $dpdComponent){
            $result[$megapolis][] = $this->inTrack($dpdComponent);
        }


        return $result;
    }
    public function processingONE($result){

        $dpd = new DPDTrackComponent();
        $dpd->dpdOrderNr        = $result->dpdOrderNr;
        $dpd->dpdParcelNr       = $result->dpdParcelNr;
        $dpd->pickupDate        = $result->pickupDate;
        $dpd->planDeliveryDate  = $result->planDeliveryDate;
        $dpd->newState          = $result->newState;
        $dpd->transitionTime    = $result->transitionTime;
        $dpd->terminalCode      = $result->terminalCode;
        $dpd->terminalCity      = $result->terminalCity;
        #$dpd->idea              = isset($result->clientOrderNr) ? $result->clientOrderNr : $result->dpdOrderReNr;

        return $dpd;
    }
    public function inTrack(DPDTrackComponent $dpd){
        $object = new $this->track;
        $date   = new DateTime($dpd->transitionTime);

        $object->address = $dpd->terminalCity;
        $object->status  = DPDsettings::status($dpd->newState);
        $object->date    = strtotime($date->format('Y-m-d H:i:s'));
        return $object;
    }
}
class DPDTrackComponent{
    #public $idea;   # Номер заказа начинающийся с DPD
    public $dpdOrderNr;
    public $dpdParcelNr;
    public $pickupDate;
    public $planDeliveryDate;
    public $newState;
    public $transitionTime;
    public $terminalCode;
    public $terminalCity;
}
class DPDsettings{
    public static $STATUS_NewOrderByClient      = 'NewOrderByClient';     # оформлен новый заказ по инициативе клиента
    public static $STATUS_NotDone               = 'NotDone';              # заказ отменен
    public static $STATUS_OnTerminalPickup      = 'OnTerminalPickup';     # посылка находится на терминале приема отправления
    public static $STATUS_OnRoad                = 'OnRoad';               # посылка находится в пути (внутренняя перевозка DPD)
    public static $STATUS_OnTerminal            = 'OnTerminal';           # посылка находится на транзитном терминале
    public static $STATUS_OnTerminalDelivery    = 'OnTerminalDelivery';   # посылка находится на терминале доставки
    public static $STATUS_Delivering            = 'Delivering';           # посылка выведена на доставку
    public static $STATUS_Delivered             = 'Delivered';            # посылка доставлена получателю
    public static $STATUS_Lost                  = 'Lost';                 # посылка утеряна
    public static $STATUS_Problem               = 'Problem';              # с посылкой возникла проблемная ситуация
    public static $STATUS_ReturnedFromDelivery  = 'ReturnedFromDelivery'; # посылка возвращена с доставки
    public static $STATUS_NewOrderByDPD         = 'NewOrderByDPD';        # оформлен новый заказ по инициативе DPD

    public static function status($data){
        switch($data){
            case self::$STATUS_NewOrderByClient:    $result = 'оформлен новый заказ по инициативе клиента';
                break;
            case self::$STATUS_NotDone:             $result = 'заказ отменен';
                break;
            case self::$STATUS_OnTerminalPickup:    $result = 'посылка находится на терминале приема отправления';
                break;
            case self::$STATUS_OnRoad:              $result = 'посылка находится в пути (внутренняя перевозка)';
                break;
            case self::$STATUS_OnTerminal:          $result = 'посылка находится на транзитном терминале';
                break;
            case self::$STATUS_OnTerminalDelivery:  $result = 'посылка находится на терминале доставки';
                break;
            case self::$STATUS_Delivering:          $result = 'посылка выведена на доставку';
                break;
            case self::$STATUS_Delivered:           $result = 'заказ посылка доставлена получателю';
                break;
            case self::$STATUS_Lost:                $result = 'посылка утеряна';
                break;
            case self::$STATUS_Problem:             $result = 'с посылкой возникла проблемная ситуация';
                break;
            case self::$STATUS_ReturnedFromDelivery:$result = 'посылка возвращена с доставки';
                break;
            case self::$STATUS_NewOrderByDPD:       $result = 'оформлен новый заказ по инициативе';
                break;
            default:
                $result = NULL;
        }
        return $result;
    }
}
class DPDException extends CException {
    public static $REQUEST_NOT_ANSWER = 'DPD. Нет ответа. Нужно повторить запрос позже!';
    public static $NOT_FIND_START    = 'DPD. Не хватает данных для запуска трекинга.';
    public static $REQUEST_NOT_TRACK  = 'DPD. Нет трекинга.';

}
class DPDNotFormatException extends DPDException {}
class DPDNotTrackException extends DPDException {
	public $code = 2000;
}