<?php

class PR {
    public static $PUT_TICKET = 'PUT_TICKET';
    public static $GET_TICKET = 'GET_TICKET';

    public $ticket;
    public $data;
    public $end; # окончание

    public function start(TrackComponent $track, $data){

        if(!$this->filter($data)) return null;
        if(!$history = PRXml::get($this->ticket)->getOperation($this->data)) return null;

        if($this->ticket == self::$PUT_TICKET){
            $this->end = new PUT_TICKET();
            $this->end->ticket = $history;
            $this->end->agents = $this->data;
            print_r($this->end);
            exit;
        }elseif($this->ticket == self::$GET_TICKET){
            $this->end = new GET_TICKET();
            $this->end->ticket = $this->data;
            $this->end->request = "$history";

        }
    }

    public function process(){
        return true;
    }
    # Дефолтные данные
    public function filter($data){
        $ticket = $data['request'];
        if($ticket == self::$GET_TICKET){
            $this->ticket = self::$GET_TICKET;
            $this->data = $data['data'][0];
        }elseif($ticket == self::$PUT_TICKET){
            $this->data = $data['data'];
            $this->ticket = self::$PUT_TICKET;
        }else{
            return NULL;
        }

        return TRUE;
    }

    public function end(){
        if(get_class($this->end) == self::$GET_TICKET){

            $text = <<<XML

XML;

            print_r($this->end->request);
            exit;
            $db = new DBtrack_pr_ticket_value;
            $db->code = 1;
            $db->dateCreate = date('Y-m-d H:i:s');
            $db->text = $this->end->request;
            $db->insert();
        }elseif(get_class($this->end) == self::$PUT_TICKET){
            print 'end';
            exit;
        }

        exit;
    }
}

class PRXml{
    public $request;
    private static $instance;
    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {}

    public function get($request){
        $ins = empty(self::$instance) ? self::$instance = new self() : self::$instance;
        $ins->request = $request;
        return $ins;
    }
    const SOAPEndpoint = 'http://r00vfc2.main.russianpost.ru:8080/FederalClientService-web/ItemDataService?wsdl';
    public function getOperation($data){

        $data = $this->request($data);
        //$data = $this->makeRequest($data);
        $data = <<<EOD
<?xml version='1.0' encoding='UTF-8'?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><ns3:answerByTicketResponse xmlns:ns2="http://fclient.russianpost.org" xmlns:ns3="http://fclient.russianpost.org/postserver"><error ErrorTypeID="6" ErrorName="Ответ для ФК ещё не готов"/></ns3:answerByTicketResponse></S:Body></S:Envelope>
EOD;

        // Тестирование данных
        /*
        $data = <<<EOD
<?xml version='1.0' encoding='UTF-8'?><S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/"><S:Body><ns3:ticketResponse xmlns:ns2="http://fclient.russianpost.org" xmlns:ns3="http://fclient.russianpost.org/postserver"><value>20150216164109178ZHUMITMIH</value></ns3:ticketResponse></S:Body></S:Envelope>
EOD;
*/
        $data = $this->parseResponse($data);

        return $data;
    }
    public function request($data){
        if($this->request == PR::$GET_TICKET){
            $result = <<<EOD
<S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    <S:Body>
        <ns3:answerByTicketRequest
                xmlns:ns2="http://fclient.russianpost.org"
                xmlns:ns3="http://fclient.russianpost.org/postserver">
            <ticket>$data</ticket>
            <login>zhumitMih</login>
            <password>zhukhTPHz</password>
        </ns3:answerByTicketRequest>
    </S:Body>
</S:Envelope>
EOD;
        }elseif($this->request == PR::$PUT_TICKET){
            if(true){
                $items = '';

                foreach($data as $k=>$v){
                    $items.='
                                <ns2:Item Barcode="'.$v.'"/>';
                }
                $date = date('d.m.Y H:i:s');
            }
            $result = <<<EOD
            <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
                <S:Body>
                    <ns3:ticketRequest
                        xmlns:ns2="http://fclient.russianpost.org"
                        xmlns:ns3="http://fclient.russianpost.org/postserver">
                        <request DatePreparation="$date"
                            FileNumber="2"
                            FileTypeID="1"
                            FileName=" ">
                        $items
                        </request>
                        <login>zhumitMih</login>
                        <password>zhukhTPHz</password>
                        <language>RUS</language>
                    </ns3:ticketRequest>
                </S:Body>
            </S:Envelope>
EOD;
        }

        return $result;
    }

    protected function makeRequest($data) {
        $channel = curl_init(self::SOAPEndpoint);
        #header("Content-Type: text/xml");

        curl_setopt_array($channel, array(
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_POSTFIELDS     => $data,
            CURLOPT_HTTPHEADER     => array(
                'Content-Type: text/xml; charset=utf-8',
                'SOAPAction: ""',
            ),
        ));

        $result = curl_exec($channel);
        print_r($result);
        exit;
        return $result;
    }

    protected function parseResponse($raw){
        $xml = simplexml_load_string($raw);

        $ns = $xml->getNamespaces(true);
        if($this->request == PR::$PUT_TICKET){
            if($value = $xml->children($ns['S'])->children($ns['ns3'])->children()->value){
                return (string)$value;
            }
            return NULL;
        }elseif($this->request == PR::$GET_TICKET){
            $value = $xml->children($ns['S'])->children($ns['ns3'])->children();

            print_r((array)$value->attributes());
            exit;
            return $raw;
        }
    }
}

class PUT_TICKET{ # Получить тикет
    public $ticket;
    public $agents;
}
class GET_TICKET{ # Вернуть от все что в тикете
    public $ticket;
    public $request;
}

/*
<?xml version='1.0' encoding='UTF-8'?>
    <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
        <S:Body>
            <ns3:answerByTicketResponse xmlns:ns2="http://fclient.russianpost.org" xmlns:ns3="http://fclient.russianpost.org/postserver">
                <value FileName=" " FileTypeID="2" FileNumber="2" RecipientID="1" DatePreparation="18.02.2015 15:39:47">
                    <ns2:Item Barcode="EC006906433RU">
                        <ns2:Operation OperTypeID="2" OperCtgID="1" OperName="Вручение" DateOper="28.03.2014 09:57:00" IndexOper="678723"/>
                    </ns2:Item>
                    <ns2:Item Barcode="EC006906478RU">
                        <ns2:Operation OperTypeID="2" OperCtgID="1" OperName="Вручение" DateOper="22.03.2014 09:30:00" IndexOper="694240"/>
                    </ns2:Item>
                    <ns2:Item Barcode="EC006906549RU">
                        <ns2:Operation OperTypeID="2" OperCtgID="1" OperName="Вручение" DateOper="19.03.2014 13:07:00" IndexOper="694450"/>
                    </ns2:Item>
                    <ns2:Item Barcode="EC006906566RU">
                        <ns2:Operation OperTypeID="2" OperCtgID="1" OperName="Вручение" DateOper="17.03.2014 16:02:00" IndexOper="694450"/>
                    </ns2:Item>
                    <ns2:Item Barcode="EC006906583RU">
                        <ns2:Operation OperTypeID="2" OperCtgID="1" OperName="Вручение" DateOper="18.03.2014 18:59:00" IndexOper="694450"/>
                    </ns2:Item>
                    <ns2:Item Barcode="EC006906597RU">
                        <ns2:Operation OperTypeID="2" OperCtgID="1" OperName="Вручение" DateOper="28.03.2014 16:55:00" IndexOper="678895"/>
                    </ns2:Item>
                    <ns2:Item Barcode="EC006906800RU">
                        <ns2:Operation OperTypeID="2" OperCtgID="1" OperName="Вручение" DateOper="20.03.2014 13:00:00" IndexOper="666523"/>
                    </ns2:Item>
                    <ns2:Item Barcode="EC006907028RU">
                        <ns2:Operation OperTypeID="8" OperCtgID="2" OperName="Обработка" DateOper="27.07.2014 02:14:00" IndexOper="130100"/>
                    </ns2:Item>
                    <ns2:Item Barcode="EC006907031RU">
                        <ns2:Operation OperTypeID="2" OperCtgID="1" OperName="Вручение" DateOper="22.04.2014 09:40:00" IndexOper="682560"/>
                    </ns2:Item>
                </value>
            </ns3:answerByTicketResponse>
        </S:Body>
    </S:Envelope>
*/