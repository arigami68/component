<?php

class AGENT {
    private $dbResult;
    public function start(TrackComponent $track){
        $track_full         = new TrackFull();
        $track_headcomponent= new TrackHeadComponent();

        if(!$result = $this->dbResult = TO1C::model()->findAll("status_track=:status", array('status' => TO1C::$TRACK_EMPTY))) throw new AGENTNotTrackException();
        $parsing   = new AGENTParsing(new StepComponent);
        $steps = $parsing->step($result);

        $array = array();
        if($steps){
            foreach($steps as $agent => $value){
                $full      = clone $track_full;
                if($order = DBorders::model()->agent($agent)->find()){

                    $flag = 1; # Это отправление есть в Нашей базе
                    $full->agent = $agent;
                    $full->dbOrder = $order;

                    $full->step = $value;
                    if(true){
                        $head = clone $track_headcomponent;
                        $head->dbOrder = $order;
                        $head->np = $order->costDelivery;
                        $head->oc = $order->costPublic;
                        $head->weight = $order->weightEnd;
                        $head->code = $order->agent;
                        $head->address = $order->address;
                        $head->zip = $order->zip;
                    }
                    $full->head = $head;
                    $array[] = $full;
                }else{
                    $HTTP_HOST = $_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];

                    $errors = new DB3errors();
                    $errors->agent = $agent;
                    $errors->code = 11;
                    $errors->text = json_encode(array('url' => $HTTP_HOST));
                    $errors->dateCreate = date('Y-m-d H:i:s');
                    $errors->insert();
                    $flag = 0; # Этого отправления нет в нашей базе
                }
            }
        }
        return $array;
    }
    # Устанавливаем отметку
    public function process(TrackFull $track){
        foreach($this->dbResult as $value){
            $agent = $value->agent;
            if($agent == $track->dbOrder->agent){
                $value->status_track = TO1C::$TRACK_SUCCESSFULLY;
                $value->update();
            }
        }
        return TRUE;
    }
    public function end(){
        # Отчет убрал в связи с ненадобностью
    }

}
class AGENTParsing{
    public $track;
    public function __construct(StepComponent $step){
        $this->track = $step;
    }
    public function step($data){
        $array = array();

        foreach($data as $value){
            $array[] = $this->component($value, new AgentTrackComponent);
        }
        foreach($array as $value){
            $agent = $value->agent;
            $track[$agent][] = $this->inTrack($value);
        }

        return $track;
    }
    public function component($value, AgentTrackComponent $agent){

        $agent->agent     = $value->agent;
        $agent->megapolis = $value->megapolis;
        $agent->date      = $value->date;
        $agent->status    = $value->status;
        $agent->comment   = $value->comment;

        return $agent;
    }
    public function inTrack(AgentTrackComponent $agent){
        $object = new $this->track;
        $date   = new DateTime($agent->date);

        $object->address    = '';
        $_status = DefaultComponent::get()->HandBook(DefaultComponent::$HANDBOOK_SETTINGSDELIVERYSTATUS);
        if(!isset($_status[$agent->status])) throw new AGENTSTATUSException;
        $status = $_status[$agent->status];

        $object->status     = $status['status'];
        $object->operation  = $status['operation'];
        $object->date       = strtotime($date->format('d.m.Y H:i'));
        $object->comment    = $agent->comment;

        return $object;
    }
}
class AgentTrackComponent{
    public $agent;
    public $megapolis;
    public $date;
    public $status;
    public $comment;
}

class AGENTException extends CException {
    public static $REQUEST_NOT_ANSWER = 'AGENT. Нет ответа. Нужно повторить запрос позже!';
    public static $REQUEST_NOT_TRACK  = 'AGENT. Нет трекинга.';

}
class AGENTNotFormatException extends AGENTException {}
class AGENTNotTrackException extends AGENTException {}
class AGENTSTATUSException extends AGENTException {}