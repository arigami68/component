<?php
const M_USER = '3f3bcb8673ae72028a968ecec260c54d';
const M_PASS = '96909a381283213a47191e3f042fadc0';
const M_BARCODE_START = '26154831';
const M_BARCODE_END = '26169830';

class MAXIPOST{
    public $order;
    public function __construct($data){
        if(get_class($data['agent']) == 'ORDS'){
            $this->order = $data['agent'];
        }else{
            throw Exception;
        }
    }
    public function start(TrackComponent $track, $data){
        if(!$history = MAXIXml::get()->getOperation($this->order)) return null;
        if(true){
            $track_full         = new TrackFull();
            $track_headcomponent= new TrackHeadComponent();
        }

        $full      = clone $track_full;
        $agent = $this->order->agent;
        $full->agent = $agent;
        $full->dbOrder = $this->order;

        if(true){
            $head = clone $track_headcomponent;
            $head->dbOrder = $this->order;
            $head->np = $this->order->costDelivery;
            $head->oc = $this->order->costPublic;
            $head->weight = $this->order->weightEnd;
            $head->code = $this->order->agent;
            $head->address = $this->order->address;
            $head->zip = $this->order->zip;

            $full->head = $head;
        }


        $full->step = $history;
        $array[] = $full;

        return $array;
    }
    public function process(){
        return TRUE;
    }
    public function end(){
    }
}

class MAXIXml{
    public $request;
    private static $instance;
    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {}

    private $url = 'http://www.maxipost.ru/api/get_status.php?';
    private $getUrl;

    public static function get(){
        return empty(self::$instance) ? self::$instance = new self() : self::$instance;}

    public function getOperation($order){

        if(true){
            $date = date('d-m-Y').'T00:00:00';
            $secure = $date.'&'.M_PASS;
            $get = 'account='.M_USER.'&secure='.md5($secure);
            $add = '&date_start='.$date;
            $this->getUrl = $this->url.$get.$add.'&recno='.urlencode($order->megapolis);
        }

        $data = $this->request($this->getUrl);
        $data = $this->parseResponse($data, $order);

        return $data;
    }

    protected function request($data) {
        $channel = curl_init($data);
        curl_setopt_array($channel, array(
            CURLOPT_POST           => true,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_POSTFIELDS     => '',
            CURLOPT_HTTPHEADER     => array(
                'Content-Type: text/xml; charset=utf-8',
            ),
        ));

        $result = curl_exec($channel);

        return $result;
    }

    protected function parseResponse($raw, $order){
        $xml = new SimpleXMLElement($raw);

        if(!isset($xml->Otpravka->Status->StatusHistory)) throw new RussianPostDataException("Нет трекинга", 1003);
        $records = $xml->Otpravka->Status->StatusHistory;

        $out = array();
        foreach($records as $rec){
            $rec['StatusId'] = (int)$rec['StatusId'];

            $empty = '-';
            $address = $empty;
            if($rec['StatusId'] == 3){
                $address = (string)$rec['StatusComment'];
            }
            $zip = 0;

            $rec['StatusDate'] = (string)$rec['StatusDate'];
            $date=new DateTime($rec['StatusDate']);
            $date = (int)$date->format('U');
            $date = date('c', $date);

            $date = new DateTime($date);
            $outRecord                      = new StepComponent;
            $outRecord->date                = strtotime($date->format('Y-m-d H:i:s'));
            $outRecord->zip                 = $zip;
            $outRecord->address             = $address;
            $outRecord->operation           = $empty;
            $outRecord->status              = (string) $rec['StatusName'];

            if(false){
                $outRecord->agent               = $order->agent;
                if($order){
                    $first_zip = $order->zip;
                    $first_address = $order->address;
                    $weight = $order->weightEnd;
                    $oc = $order->costPublic;
                    $np = $order->costDelivery;
                }

                $outRecord->first_zip       = $first_zip;
                $outRecord->first_address   = $first_address;
                $outRecord->weight          = $weight;
                $outRecord->oc              = $np;
                $outRecord->np              = $oc;
            }
            $out[]                      = $outRecord;
        }

        return $out;
    }
}