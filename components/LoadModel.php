<?php
# Загрузка по API отправлений
class LoadModel extends CModel{
    public static $OPERATION_INSERT = 'insert';
    public static $OPERATION_UPDATE = 'update';
    public static $OPERATION_DELETE = 'delete';
    
    public static $CODING_JSON = 'json';
    
    public static $TYPE_XML = 'xml';
    public static $TYPE_ARRAY = 'array';
    
    # Формат сообщения
    public $type = 'xml';
    # Кодирование сообщения
    public $coding = 'json';
    
    public $operation = NULL;
    # Операции
    # (insert OR update) AND delete AND insert;
    # 
    private $data = array();
    public function attributeNames(){}
    # Класс для работы с Клиентом
    public function setCLIE(CLIEComponent $CLIE){$this->CLIE = $CLIE;}
    public function setOutput(OutputModel $output){ $this->output= $output;}
    # Экземляр добавления данных. Например одна XML
    public function setData($data){$this->data[] = $data;}
    public function getData(){return $this->data;}
    # Какую операцию проводить
    public function operation($name){
        switch ($name){
            case self::$OPERATION_INSERT:
                $operation = self::$OPERATION_INSERT;
                break;
            case self::$OPERATION_UPDATE:
                $operation = self::$OPERATION_UPDATE;
                break;
            case self::$OPERATION_DELETE:
                $operation = self::$OPERATION_DELETE;
                break;
            default:
                $operation = NULL;
                break;
        }
        return $operation;
    }
    public function beforeValidate(){
        return $this->errors ? TRUE : FALSE;
    }
    public function decode($data){
        if($this->coding == self::$CODING_JSON){
            return json_decode($data);
        }else{
            return $data;
        }
        return NULL;
    }
    public static function doHash($str, $num = 8){
        return (int)substr(preg_replace("/[a-zA-Z]/si" ,'',sha1(self::doNormalString($str))),0,$num);
    }
    public static function doNormalString($str){
        return (string)mb_strtolower(trim((string)$str) , 'UTF-8' );
    }
}