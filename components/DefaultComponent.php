<?php

/**
 * Class DefaultComponent :: Общий Справочник
 */
class DefaultComponent{
    public static $HANDBOOK_DELIVERY_STATUS         = 'delivery_status';# Отключен :: Проверить и удалить
    public static $HANDBOOK_DELIVERY_TYPE           = 'delivery_type';  # Отключен :: Проверить и удалить
    public static $HANDBOOK_REQUESTS_STATUS_CHAT    = 'requests_chat';  # Статусы в Чате
    public static $HANDBOOK_SETTINGSMEGAPOLISSTATUS = 'status';         # Получение статусов
    public static $HANDBOOK_CALCTYPE                = 'calctype';       # Виды отправлений
    public static $HANDBOOK_OD                      = 'od'; # Статусы отправлений IdeaLogic
	public static $HANDBOOK_SHORTNAMESMS 			= 'shortNameSMS';

    public static $HANDBOOK_SETTINGSDELIVERYSTATUS  = 'settingsdeliverystatus'; # Получение Агентских статусов
	public static $HANDBOOK_ALL  					= 'settingsdeliverystatus'; # Получение Агентских статусов
	public static $HANDBOOK_FLAGIDENT 				= 'flagident';

    private static $instance;
    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {}

    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}
    public function HandBook($obj = NULL){
        $result = NULL;
        switch($obj){
			case self::$HANDBOOK_SHORTNAMESMS:
				$data = DBhandbookShortNameSMS::model()->findAll();
				foreach($data as $value){
					$result[] = $value;
				}
				break;
            case self::$HANDBOOK_DELIVERY_STATUS:
                $data = DBsettingsdeliverystatus::model()->findAll();
                foreach($data as $value){
                    $key = $value->id;
                    $value = $value->status;
                    $result[$key] = $value;
                }
            break;
            case self::$HANDBOOK_DELIVERY_TYPE:
                $data = DBsettingsdeliverytype::model()->findAll();
                foreach($data as $value){
                    $key = $value->id;
                    $value = $value->deliveryType;
                    $result[$key] = $value;
                }
            break;
            case self::$HANDBOOK_REQUESTS_STATUS_CHAT:
                $result[0] = 'открыт';
                $result[1] = 'закрыт';
            break;
            case self::$HANDBOOK_CALCTYPE:
                $data = DBcalctype::model()->findAll();
                foreach($data as $value){
                    $key = $value->id;
                    $value = $value->type;
                    $result[$key] = $value;
                }
            break;
            case self::$HANDBOOK_SETTINGSMEGAPOLISSTATUS:
                $data =  DBhandbookAllStatus::model()->findAll();
                foreach($data as $value){
                    $key = $value->id;
                    $value = $value->status;
                    $result[$key] = $value;
                }
            break;
            case self::$HANDBOOK_OD:
                $data =  CLOD::model()->findAll();
                foreach($data as $value){
                    $key = $value->id;
                    $value = $value->od;
                    $result[$key] = $value;
                }
            break;
            case self::$HANDBOOK_SETTINGSDELIVERYSTATUS:
                $data =  DB2settingsdeliverystatus::model()->findAll();
                foreach($data as $value){
                    $key                        = $value->id;
                    $status                     = $value->status;
                    $operation                  = $value->operation;
                    $result[$key]['status']     = $status;
                    $result[$key]['operation']  = $operation;
                }
            break;
			case self::$HANDBOOK_FLAGIDENT:
				$data =  DBhandbookFlagIdent::model()->findAll();
				foreach($data as $value){
					$result[$value->id] = $value;
				}
			break;
        };

        return $result;
    }
    /**
     * @comment Возвращает сумму прописью
     */
    public static function num2str($num) {
        $nul='ноль';
        $ten=array(
            array('','один','два','три','четыре','пять','шесть','семь', 'восемь','девять'),
            array('','одна','две','три','четыре','пять','шесть','семь', 'восемь','девять'),
        );
        $a20=array('десять','одиннадцать','двенадцать','тринадцать','четырнадцать' ,'пятнадцать','шестнадцать','семнадцать','восемнадцать','девятнадцать');
        $tens=array(2=>'двадцать','тридцать','сорок','пятьдесят','шестьдесят','семьдесят' ,'восемьдесят','девяносто');
        $hundred=array('','сто','двести','триста','четыреста','пятьсот','шестьсот', 'семьсот','восемьсот','девятьсот');
        $unit=array( // Units
            array('копейка' ,'копейки' ,'копеек',	 1),
            array('рубль'   ,'рубля'   ,'рублей'    ,0),
            array('тысяча'  ,'тысячи'  ,'тысяч'     ,1),
            array('миллион' ,'миллиона','миллионов' ,0),
            array('миллиард','милиарда','миллиардов',0),
        );
        //
        list($rub,$kop) = explode('.',sprintf("%015.2f", floatval($num)));
        $out = array();
        if (intval($rub)>0) {
            foreach(str_split($rub,3) as $uk=>$v) { // by 3 symbols
                if (!intval($v)) continue;
                $uk = sizeof($unit)-$uk-1; // unit key
                $gender = $unit[$uk][3];
                list($i1,$i2,$i3) = array_map('intval',str_split($v,1));
                // mega-logic
                $out[] = $hundred[$i1]; # 1xx-9xx
                if ($i2>1) $out[]= $tens[$i2].' '.$ten[$gender][$i3]; # 20-99
                else $out[]= $i2>0 ? $a20[$i3] : $ten[$gender][$i3]; # 10-19 | 1-9
                // units without rub & kop
                if ($uk>1) $out[]= self::morph($v,$unit[$uk][0],$unit[$uk][1],$unit[$uk][2]);
            } //foreach
        }
        else $out[] = $nul;
        $out[] = self::morph(intval($rub), $unit[1][0],$unit[1][1],$unit[1][2]); // rub
        $out[] = $kop.' '.self::morph($kop,$unit[0][0],$unit[0][1],$unit[0][2]); // kop
        return trim(preg_replace('/ {2,}/', ' ', join(' ',$out)));
    }

    /**
	 * @comment Склоняем словоформу
     */
    public static function morph($n, $f1, $f2, $f5) {
        $n = abs(intval($n)) % 100;
        if ($n>10 && $n<20) return $f5;
        $n = $n % 10;
        if ($n>1 && $n<5) return $f2;
        if ($n==1) return $f1;
        return $f5;
    }
}