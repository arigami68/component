<?php

abstract class MegapolisRatingWidgetAbstract extends CWidget implements Collection{
    private $array = array();
    public function pull(){ return $this->array;}
    public function push($array){ return array_push($this->array, $array);}
    public function reset(){ $this->array = array();}
}
class MegapolisRatingWidget extends MegapolisRatingWidgetAbstract{
	public $date     = NULL;
	public $contract = NULL;
	public $ident = 1;
    public function run(){
        if(!self::getSee(DBrating::getSeeCriteria(), $this->date, $this->contract)) return false;
        if(!$this->getQuestion()) return false;

        print $this->view();
    }
	public function view($text=''){
		$text.=$this->subject();
		$text.=$this->body($this->pull());
		$text.=$this->additions();
		return $text;
	}
    public function subject(){
        $subject = "
        ";
        return $subject;
    }
    public function body($array){
        $body = "<div class='questions'>";
		$body.= "<div class='subject'>Ваши оценки:</div>";
		$body.= "<br>";
        foreach($array as $value){
            $body.= "<div>";
            $body.= "<div class='question' id=".$value->id.">".$value->question."</div>";
            $body.= $this->getAnswer($value->answer);
            $body.= "</div>";
        }

        $body.='</div>';
		$body.= "<div class='clear'></div>";
		$body.= "<div class='subject'>Ваш комментарий:</div>";
		$body.= "<br>";
		$body.= "<textarea id=6></textarea>";
		$body.= "<br><br>";

        return $body;
    }
    public function getAnswer($flag){
        $text = '';
        if($flag == 1){
            $text= '<div class="answer">
                        <ul >
                            <li class="current" style="width: 0%"><span class="star1" >Совсем&nbspплохо</span></li>
                            <li><span class="star2" >Плохо</span></li>
                            <li><span class="star3" >Удовлетворительно</span></li>
                            <li><span class="star4" >Хорошо</span></li>
                            <li><span class="star5" >Вы&nbspнеповторимы</span></li>
                        </ul>
                    </div>';
        }
        return $text;
    }
    public function getQuestion(){

        $array = DBratingquestion::model()->ident($this->ident)->work()->findAll();
        foreach($array as $value){ $this->push($value);}

        return $this->pull();
    }

	/**
	 * Проверка доступности опроса
	 * @param $criteria
	 * @param $date
	 *
	 * @return bool
	 */
	public static function getSee($criteria, $date, $contract){

        $dateCreate = $date;
        $dcreate = strtotime($dateCreate) + (3600 * 24 * 30);

        if($dcreate > time()) return false;

        if($rating = DBrating::model()->contract($contract)->find($criteria)){
            $r = function($date){
                $s = strtotime($date);
                $date = new DateTime();
                $date->setTimestamp($s);

                $Y = $date->format('Y');
                $m = $date->format('m');
                $d = $date->format('d');

                $new = new DateTime();

                $new->setTimestamp(time());
                $st = strtotime($date->format('Y-m-d'));
                $sn = strtotime($new->format('Y-m-05'));


                if($sn > $st){
                    return false;
                }
                return true;
            };

            return $r($rating->dateCreate);
        }
        return true;
    }
    public function additions(){
		$add = '';

        $add.= '<div class="question-status">
        	<button class="btn btn-primary saveQ" flag="save">Сохранить</button>
        	<button class="btn btn-primary closeQ" flag="close">Отказаться от опроса</button>
        </div>';
		#$add.= '<div class="closeQ"><a href="#">Закрыть</a></div>';
        return $add;
    }
}