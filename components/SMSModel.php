<?php

/**
 * Работа с SMS
 */

class SMSModel extends CModel{
	public function attributeNames(){}
	/**
	 * Получаем все заказы от Клиента по которым нужно отправить SMS
	 * @param CDbCriteria $criteria
	 * @return array|CActiveRecord|CActiveRecord[]|mixed|null
	 */
	public function getOrders(CDbCriteria $criteria){
		$array = array();

		if($orders =  DBorders::model()->findAll($criteria)){

			foreach($orders as $k=>$v){
				$array[$v->agent] = $v;
			}
			$criteria->offset+=$criteria->limit;
		}

		return $array;
	}

	/**
	 * Получить статусы заказов
	 * @param $orders
	 * @return array|bool|CActiveRecord|CActiveRecord[]|mixed|null
	 */
	public function getStatus(array $orders){
		$array = array();
		foreach($orders as $value){
			$array[] = $value->agent;
		}
		if(!$array) return false;

		$criteria = new CDbCriteria;
		$criteria->addInCondition('code', $array);
		DBtrace::setCriteriaCurrentDate($criteria);

		$trace = DBtrace::model()->findAll($criteria);
		return $trace;
	}

	/**
	 * Получить все шаблоны
	 * @param $criteria
	 * @return array|CActiveRecord|CActiveRecord[]|mixed|null
	 */
	public function getTemplate($criteria){
		$template = DBSmsStatus::model()->findAll($criteria);

		return $template;
	}

	/**
	 * Получаем шаблон для отправки SMS
	 * @param $status
	 * @param $temp
	 * @param $orders
	 */
	public function processTemplate($status, $temp, $orders){
		$short = HANDBOOK::handbookShortNameSMS();
		$text = $temp->text;

		foreach($short as $k=>$value){
			switch($value->short){
				case '#idealogic#':
					$text = str_replace('#idealogic#', $orders->agent, $text);
					break;
				case '#number#':
					$text = str_replace('#number#', $orders->phone, $text);
					break;
				case '#fio#':
					$text = str_replace('#fio#', $orders->fio, $text);
					break;
				case '#od#':
					$text = str_replace('#od#', $orders->handbookOd->od, $text);
					break;
				case '#status#':
					$text = str_replace('#status#', $status->handbookallstatus->status, $text);
					break;
			}
		}

		return $text;
	}

	/**
	 * Записать SMS
	 * @param $array
	 */
	public function setSMS($array){

		$smsReport = new DBSmsStatusReport;

		$smsReport->agent 		= $array['agent'];
		$smsReport->megapolis 	= $array['megapolis'];
		$smsReport->status 		= $array['status'];
		$smsReport->dateRecept	= $array['dateRecept'];
		$smsReport->dateCreate	= $array['dateCreate'];
		$smsReport->contract 	= $array['contract'];
		$smsReport->num 		= $array['number'];
		$smsReport->OC 			= $array['OC'];
		$smsReport->NP 			= $array['NP'];
		$smsReport->phone 		= $array['phone'];
		$smsReport->idTrace 	= $array['idTrace'];
		$smsReport->insert();

	}
	/**
	 * Проверка SMS. Нужно ли отправлять
	 * @param $array
	 */
	public function checkSMS($array){
		$agent 	= $array['agent'];
		$phone 	= $array['phone'];
		$num	= $array['number'];

		$contract	= $array['contract'];

		$report = DBSmsStatusReport::model()->unique($agent, $phone, $num)->contract($contract)->find();

		if($report) return false;
		if($num == 0){return true;
		}else{

			// Должна ли быть отправлена n-ая SMS
			if($reportNum = DBSmsStatusReport::model()->unique($agent, $phone, 0)->contract($contract)->find()){
				$old = strtotime(date('Y-m-d', strtotime($reportNum->dateRecept))) + $num*3600*24;
				$new = time();
				if($new> $old){
					$criteria = new CDbCriteria();
					$criteria->order = 'id DESC';
					$trace = DBtrace::model()->idOrder($array['idOrder'])->find($criteria);
					if($trace->id === $array['idTrace']){ // @comment Если есть следующий статус, то вторую смс не отсылаем
						return true;
					}else{
						return false;
					}
				}
			}
		}
		return false;
	}
}