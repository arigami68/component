<?php

class TrackComponent
{
    public $id;
    public $r;
    public $data;
    public static $checkRepeat = 1; # Проверка текущего трекинга. Если есть такой же трекинг, то мы удаляем повторы
    
    public $code;
    public $weight;
    public $np;
    public $oc;
    public $zip;
    public $address;
    public $step;

	/**
	 * Не использовать
	 */
	public function convert(ORDS $order){
        if( !isset($order->track->data)) throw new CException;

        $data = $order->track->data;
        $data = str_replace('u0', '\u0', $data);

        $data = json_decode($data);

        if(!isset($data->code)) throw new Exception('Нет ШПИ');

        $this->code     = $data->code;
        $this->weight   = $data->weight;
        $this->np       = $data->np;
        $this->oc       = $data->oc;
        $this->zip      = $data->zip;
        $this->address  = $data->address;

        if($data->step){
            foreach($data->step as $k=>$v){
                $step               = new StepComponent;
                $step->date         = $v->date;
                $step->zip          = $v->zip;
                $step->address      = $v->address;
                $step->operation    = $v->operation;
                $step->status       = $v->status;
                $step->comment      = isset($v->comment) ? $v->comment : '';
                $this->step[]       = clone $step;
            }
        }
        return $this;
    }

	public function setAttribute($data){
		$this->id       = $data['idOrder'];
		$this->code     = $data['agent'];
		$this->weight   = $data['weight'];
		$this->np       = $data['np'];
		$this->oc       = $data['oc'];
		$this->zip      = $data['zip'];
		$this->address  = $data['address'];

		return true;
	}

	/**
	 * Сборка трекинга из Таблицы Trace
	 */
	public function getAssemblyTrace(){

		if($db = DBtrace::model()->idOrder($this->id)->findAll()){

			foreach($db as $k=>$v){
				$step               = new StepComponent;
				$step->date         = strtotime($v->dateD);
				$step->zip          = $this->zip;
				$step->address      = $v->address;
				$step->operation    = $v->operation;
				$step->status       = $v->status;
				$step->comment      = $v->comment;
				$this->step[]       = clone $step;
			}
		}
		return $this;
	}
	/*
	 * @name :: testArray
	 * @comemnt :: если что-то поломалось в json-е, то мы удаляем эту запись.
	 */
	public static function testArray(TrackFull $full){
		if(mb_strlen($full->dbOrder->track->data)){
			$track = $full->dbOrder->track;
			$data = str_replace('u0', '\u0', $track->data);
			$data = json_decode($data);
			if($data === null){
				$track->data = '';
				$track->update();
			}
		}
	}
    # Добавлено 22.12.14
    public static function processingInArray(TRA2 $track){

        $data = $track->data;
        $data = str_replace('u0', '\u0', $data);

        $data = json_decode($data);

        $full = new TrackFull();
        if(!$data) return false;


        $full->agent = $data->code;

        if(true){
            $head = new TrackHeadComponent();
            $head->code     = $data->code;
            $head->weight   = $data->weight;
            $head->np       = $data->np;
            $head->oc       = $data->oc;
            $head->zip      = $data->zip;
            $head->address  = $data->address;
            $head->done     = $data->done;
            $full->head = $head;
        }
        $array = Array();
        foreach($data->step as $value){
            $step = clone new StepComponent();
            $step->date         = $value->date;
            $step->address      = $value->address;
            $step->status       = $value->status;
            $step->zip          = $value->zip;
            $step->operation    = $value->operation;
            $step->comment      = isset($value->comment) ? $value->comment : '';
            $full->step[] = $step;
        }

        return $full;
    }
    public static function processingInTrack(TrackFull $full){
        $array = new StdClass;
        $array->code = $full->agent;
        $array->weight = $full->head->weight;
        $array->np = $full->head->np;
        $array->oc = $full->head->oc;
        $array->zip = $full->head->zip;
        $array->done = 0;#$full->head->done;
        $array->address = $full->head->address;
        $array->step = $full->step;

        $data = json_encode($array);

        $data = str_replace('\u', 'u', $data);
        return $data;
    }
    # push трек
    public static function addStep(TRA2 $track, TrackFull $data){
        $operation = (empty($track->data)) ? TrackFull::$OPERATION_NEW : TrackFull::$OPERATION_PUSH_ADD;

        if($operation == TrackFull::$OPERATION_NEW){

            $step = $data->step;
        }elseif($operation == TrackFull::$OPERATION_PUSH_ADD){
            $track = TrackComponent::processingInArray($track);

            $array = self::$checkRepeat ? TrackComponent::repeat($track, $data->step) : $data->step;

            foreach($array as $value){
                $track->step[] = $value;
            }

            $step = $data->step;
        }
        return $step;
    }
    public static function repeat(TrackFull $full,  $data = array()){
		if(!isset($full->dbOrder->track->data)) return $data;

        if(!$full->dbOrder->track->data){
            $data = $full->step;
        }else{
            $track = TrackComponent::processingInArray($full->dbOrder->track);

            $step = $track->step;
            $array = $full->step;
            foreach($array as $k=>$new){
                foreach($step as $key => $old){
                    if($new->date == $old->date){
                        unset($array[$k]);
                    }
                }
            }
            $data = $array;

        }

        return $data;
    }
}