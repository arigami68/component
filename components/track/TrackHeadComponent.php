<?php

class TrackHeadComponent{
    public $done = 0;
    public $data;

    public $code;
    public $weight;
    public $np;
    public $oc;
    public $zip;
    public $address;
}