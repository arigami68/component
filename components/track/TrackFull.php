<?php

class TrackFull extends CModel{
    public $dbOrder;        # Таблица order
    public $agent;
    public $head;
    public $step;

    public static $OPERATION_NEW = 'new';
    public static $OPERATION_PUSH_ADD = 'addPush';


    public function attributeNames(){}
    public function setHead(TrackHeadComponent $head){ $this->head = $head;}
    public function setStep(StepComponent $step){ $this->step = $step;}
}