<?php

class TrackModel extends CModel{
    public static $OD_DPD   = 'DPD';    # DPD
    public static $OD_AGENT = 'AGENT';
    public static $OD_PR    = 'PR';     # Почта России
    public static $OD_MAXI  = 'MAXIPOST';   # MaxiPost
    private $od;
    public $data;
    public function attributeNames(){}

    public function __construct($od = NULL, $data){
        $this->data = $data;
        switch($od){
            case self::$OD_MAXI:
                $track = new $od($data);
                $this->od = $track;
                break;
            case self::$OD_DPD:
            case self::$OD_AGENT:
            case self::$OD_PR:

                $track = new $od();
                $this->od = $track;
            break;
                $this->od = NULL;
        }
    }
    public function start(TrackComponent $track){
        $array = $this->od->start($track, $this->data);

        return $array;
    }
    # Добавление трекинга
    public function process($value){

		$repeat = TrackComponent::testArray($value);
        $repeat = TrackComponent::repeat($value);

        if($this->od->process($value)){
            if($repeat){ # Если есть новые значения

                $step = TrackComponent::addStep($value->dbOrder->track, $value);
				/*
                $value->step = $step;
                $array = TrackComponent::processingInTrack($value);
                $value->dbOrder->track->done = $value->head->done;
                $value->dbOrder->track->data = $array;

                $value->dbOrder->track->update();
	            */
                foreach($repeat as $stepONE){
                    $this->addTrace($stepONE, $value);
                }
            }
        }else{
            return FALSE;
        }
    }
    public function end(){
        return $this->od->end();
    }
    public function addTrace($stepONE, $value){
        if(empty($value->agent)) return false;

        $date = date('Y-m-d H:i:s', $stepONE->date);
        $date_1c = date('d.m.Y H:i', $stepONE->date);
        $zip = empty($stepONE->zip) ? '' : $stepONE->zip;

        $trace = new TRAC();
        $trace->idOrder     = $value->dbOrder->id;
        $trace->done        = $value->dbOrder->track->done;
        $trace->code        = $value->agent;
        $trace->status      = $stepONE->status;
        $trace->operation   = isset($stepONE->operation) ? $stepONE->operation : '';
        $trace->zip         = $zip;
        $trace->address     = $stepONE->address;
        $trace->date        = $date_1c;
        $trace->dateD       = $date;
        $trace->flag1c      = 0;
        $trace->dateRecord  = date('Y-m-d H:i:s');
        $trace->comment     = $stepONE->comment;

        return $trace->insert();
    }

}
class TrackOrderException extends CException {}