<?php

class Simple{
    private $tableResidue = 'trackorderresidue';

    private static $instance;
    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {}

    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}
    public function count(){
        $count = DB3trackorder::model()->lockFlag(0)->count();
        return $count;
    }
    /*
     * @name :: synchronization
     * @comment :: Добавление записей из Orders
     */
    public function add($count = 1000){
        $sql = 'SELECT o.id
                FROM logs.trackorder t
                RIGHT JOIN megapolis.orders o ON t.idOrder = o.id
                JOIN megapolis.track2 t2 ON t2.id_order = o.id
                WHERE t.idOrder IS NULL
                AND t2.done =0
                AND (
                o.od =1
                OR o.od =2
                )
                LIMIT 0 ,'.$count.'
        ';
        if(!$idOrders = Yii::app()->logs->createCommand($sql)->queryColumn()){
            return false;
        }
        $sql = 'INSERT INTO logs.trackorder ( `idOrder` ) VALUES ';
        foreach($idOrders as $idORder){
            $sql.="(".$idORder."),";
        }
        $sql = substr($sql, 0, -1);
        $command = Yii::app()->logs->createCommand($sql)->execute();
    }
    /*
     * @name :: delete
     * @comment :: Удаление тех значений, где трекинг уже не нужен
     */
    public function delete(){
        $sql = 'DELETE t FROM trackorder AS t JOIN megapolis.orders AS o ON o.id = t.idOrder WHERE idOrder IN (
                SELECT id_order
                FROM megapolis.track2
                WHERE done =1
                )
                AND (
                o.od !=1
                OR o.od !=2
                )
                OR o.od IS NULL
                ';
        return $result = Yii::app()->logs->createCommand($sql)->execute();
    }
    /*
     * @name :: transfer
     * Переносим номера в отдельную таблицу
     */
    public function transfer(){
        $sql = 'SELECT idOrder
                FROM trackorder
                WHERE flag=0
        ';

        if(!$idOrders = Yii::app()->logs->createCommand($sql)->queryColumn()){
            return false;
        }

        $sql = 'INSERT INTO '.$this->tableResidue.' ( `idOrder` ) VALUES ';
        foreach($idOrders as $idORder){
            $sql.="(".$idORder."),";
        }
        $sql = substr($sql, 0, -1);
        return $command = Yii::app()->logs->createCommand($sql)->execute();
    }
    /*
    * @name :: checkTransfer
    * Проверяем есть ли что-то в этой таблице
    */
    public function checkTransfer(){
        $sql = 'SELECT count(*) FROM '.$this->tableResidue;
        return $result = Yii::app()->logs->createCommand($sql)->queryScalar();
    }
    /*
    * @name :: getCountTransfer
    * Получаем количество значений
    */
    public function getCountTransfer($flag = 0){
        $sql = 'SELECT count(*) FROM '.$this->tableResidue.' WHERE lockFlag='.$flag;
        return $result = Yii::app()->logs->createCommand($sql)->queryScalar();
    }
	/*
	 * @name :: processingError
	 * @comment :: Обработка
	 */
	public function processingError(){

		$sql = 'SELECT idOrder FROM '.$this->tableResidue.' WHERE lockFlag=1001';
		if(!$idOrders = Yii::app()->logs->createCommand($sql)->queryColumn()){
			return false;
		}
		$criteria = new CDbCriteria;
		$criteria->addInCondition('id', $idOrders);
		$orders = ORDS::model()->findAll($criteria);

		foreach($orders as $value){
			$od = $value->od;
			if($od == 1 || $od == 2){
				$value->od = null;
				$value->update();
			}
			$this->deleteIdOrderInTrack($value->id);
		}

		return true;
	}
	public function deleteIdOrderInTrack($idOrder){
		if($DB3trackorder = DB3trackorder::model()->idOrder($idOrder)->find()){
			$DB3trackorder->delete();
		}
		if($DB3trackorderresidue = DB3trackorderresidue::model()->idOrder($idOrder)->find()){
			$DB3trackorderresidue->delete();
		}
		return true;
	}
    public function TRUNCATETable(){
        $sql = 'TRUNCATE TABLE `trackorderresidue`';
        return $command = Yii::app()->logs->createCommand($sql)->execute();
    }



    public function resetLockFlag(){
        $sql = 'UPDATE trackorder SET lockFlag= 0, flag=0, dateTrackingFlag = NULL';
        return $result = Yii::app()->logs->createCommand($sql)->execute();
    }
}
abstract class SimpleAbstract{
    abstract public function setTable();
}