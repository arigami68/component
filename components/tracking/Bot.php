<?php
class Bot{

    private $time;
    public $db;
    public static $BOT_ON = 1;     # Бот включен
    public static $BOT_OFF = 0;    # Бот отключен

    private static $instance;
    private function __construct(){
        $this->time = time();
    }
    private function __clone()    {}
    private function __wakeup()   {}

    public function attributeNames(){}
    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}

    /*
     * @name :: count
     * @comment :: Возвращаем количество Ботов
     * @output :: integer
     */
    public function count(){
        $criteria = new CDbCriteria();
        $criteria->condition = 'status ='.self::$BOT_ON;

        $count = DBtrack2_bot_bumblebee::model()->count($criteria);
        return $count;
    }
    /*
     * @name :: deleteBot
     * @comment :: Удаление зависших Ботов
     * @input :: Ограничение по времени когда вырубаем Ботов
     */
    public function delete($time_end){
        $criteria = new CDbCriteria();
        $criteria->condition = 'time <'.$time_end;
        $criteria->select = 'id, status';

        $bots = DBtrack2_bot_bumblebee::model()->status(self::$BOT_ON)->findAll($criteria);

        foreach($bots as $bot){
            $bot->status = self::$BOT_OFF;
            $bot->update();
        }
        return true;
    }
    /*
     * @name :: search
     * @comment :: Поиск свободного Бота
     * @output :: Доступный лимит
     */
    public function search($count){
        $limited = ($bots = DBtrack2_bot_bumblebee::model()->status(self::$BOT_ON)->limited('=', 0)->find()) ? NULL : 0;

        if($limited === NULL){
            $criteria = new CDBcriteria();
            $criteria->order = 'id DESC';
            $criteria->select = 'limited';
            if(!$bots = DBtrack2_bot_bumblebee::model()->status(self::$BOT_ON)->limited('!=', 0)->findAll($criteria)){
                $limited=30;
            }else{
                $array = array();
                $limit = $count;
                $criteria = new CDBcriteria();
                $criteria->select = 'limited';
                foreach($bots as $value){
                    $array[] = $value->limited;
                }
                for($a=1;$a< $limit;$a++){
                    if(!in_array($limit*$a, $array)){
                        $limited = $limit*$a;
                        break;
                    }
                }
            }
        }

        return $limited;
    }
    /*
     * @name :: add
     * @comment :: Добавление Бота
     */
    public function add(DBtrack2_bot_bumblebee $bot){

        $bot->status        = self::$BOT_ON;
        $bot->time          = $this->time;
        $bot->time_work     = 0;
        $bot->count         = 0;
        $bot->count_error   = 0;
        $bot->limited       = 0;

        return $this->db = $bot->save();
    }
}