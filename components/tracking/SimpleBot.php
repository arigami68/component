<?php

class SimpleBot{
    public $settings;
    public function __construct(SimpleBotSettings $settings){
        $this->settings = $settings;
    }
    /*
     * @name :: getOrders
     * @comment :: Отдаем id записей из Orders
     * @output :: array записей id из Orders
     */
    public function getIdOrders(){
        $count = $this->settings->count;
        $table = $this->settings->getTable();

        $sql = 'SELECT  idOrder
                FROM `'.$table.'`
                WHERE lockFlag= '.DB3trackorder::$lockFlag_OFF.'
                ORDER BY idOrder DESC
                LIMIT 0,'.$count.'
        ';

        return Yii::app()->logs->createCommand($sql)->queryColumn();
    }
    /*
     * @name :: setUpdatelockFlag
     * @comment :: Обновляем флаг у записей lockFlag
     * @input :: array записей id из Orders
     */
    public function setUpdateLockFlag($idOrders){

        $sql = 'UPDATE logs.trackorder SET lockFlag='.SimpleBotSettings::$lock_ON.' WHERE idOrder IN (';
        foreach($idOrders as $idORder){
            $sql.="(".$idORder."),";
        }
        $sql = substr($sql, 0, -1);
        $sql.=')';
        $command = Yii::app()->logs->createCommand($sql)->execute();
    }
    /*
     * @name :: getDataofIdOrder
     * @comment :: Отдаем Шаблон
     * @input :: $array - array записей id из Orders
     */
    public function getTrackHeadofIdOrders($array){
		if(!$array) return array();
        $criteria = new CDbCriteria();
        $criteria->addInCondition('id', $array);
        $criteria->select = 'id, agent, weightEnd, costPublic, costDelivery, zip, address';
        $orders = ORDS::model()->findAll($criteria);
        $array = array();
        foreach($orders as $value){
            $full = new TrackFull;
            $full->agent = $value->agent;
            $full->dbOrder = $value;
            $array[] = clone $full;
        }

        return $array;
    }
	/*
	 * @name :: getCorrect
	 * @comemnt :: Если данного ID нет в Orders, то удаляем его из Трека.
	 */
	public function getCorrect($array){
		foreach($array as $k=>&$value){


			if(!ORDS::model()->id($value)->find()){
				unset($array[$k]);
				Simple::get()->deleteIdOrderInTrack($value);
			}
		}

		return $array;
	}
    /*
     * @name :: setUpdateAgent
     * @comment :: Обновить время
     * @output :: id из Orders
    */
    public function setUpdateAgent($idOrder){
        $result = FALSE;
        if($idOrder = DB3trackorder::model()->idOrder($idOrder)->find()){
            $idOrder->dateTrackingFlag = date('Y-m-d H:i');
            $idOrder->flag      = 1;
            $idOrder->lockFlag  = 1;
            $result = $idOrder->update();
        }
        return $result;
    }
    public function setUpdateAgentRe($idOrder, $flag = 1){

        $result = FALSE;
        if($idOrder = DB3trackorderresidue::model()->idOrder($idOrder)->find()){

            $idOrder->dateUpdate = date('Y-m-d H:i');
            $idOrder->lockFlag  = $flag;

            $result = $idOrder->update();
        }

        return $result;
    }
}
