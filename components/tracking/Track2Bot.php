<?php

class Track2Bot{
    private static $instance;
    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {}

    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}

    public function add($count = 1000){
        $step = 0;

        while($step < 100000){
            $sql = 'SELECT id
                    FROM orders o ORDER BY id DESC
                    LIMIT '.$step.' , '.$count.'
            ';
            $idOrders = Yii::app()->db->createCommand($sql)->queryColumn();

            $criteria = new CDbCriteria;
            $criteria->select = 'id_order';
            $criteria->addInCondition('id_order', $idOrders);
            $tra2 = TRA2::model()->findAll($criteria);

            $idOrders_flip = array_flip($idOrders);
            unset($idOrders);

            foreach($tra2 as $value){
                $id = $value->id_order;

                if(isset($idOrders_flip[$id]))
                    unset($idOrders_flip[$id]);
            }
            $step+=$count;

            $sql = "INSERT INTO track2 ( `id_order` , `data` , `done` , `date_create` , `date_update` , `date_tracking`, `error` ) VALUES ";
            $array = array();

            foreach($idOrders_flip as $order => $value){
                $time = time();
                $array = array(
                    'id_order' => $order,
                    'data' => '',
                    'done' => 0,
                    'date_create' => $time,
                    'date_update' => $time,
                    'date_tracking' => $time,
                    'error' => 0,
                );
                $sql.="(
                                ".$array['id_order'].",
                               '".$array['data']."',
                                ".$array['done'].",
                                ".$array['date_create'].",
                                ".$array['date_update'].",
                                ".$array['date_tracking'].",
                                ".$array['error']."
                        ),";
            }
            if($array){
                $sql = substr($sql, 0, -1);
                $command = Yii::app()->db->createCommand($sql)->execute();
            }
        }
    }

}
