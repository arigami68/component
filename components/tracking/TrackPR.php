<?php
/*
 * @name :: TrackPR
 * @comment :: Почтовый Трекинг
 */
class TrackPR extends TrackAbstract{
    public $encoding = 'UTF-8';
    public function __construct(){


        $simple = new SimpleBot(new SimpleBotSettings(array('table' => 'trackorder')));
        if(!$orders = $simple->getIdOrders()) throw new TrackPRException;
        $simple->setUpdateLockFlag($orders);
        $fulls = $simple->getTrackHeadofIdOrders($orders);

        $timeStart = time();


        foreach($fulls as $full){

            $agent = $full->agent;
            try{
                $api = RussianPostAPI::get()->getOperationHistory($agent);
            }catch (Exception $e) {
                if($e->code == 1002){
                    $simple->setUpdateAgent($full->dbOrder->id);
                }else{
                    $countError++;
                }
                continue;
            }

            $all = $this->toTrackComponent($api, $full);
            $model = new TrackModel(TrackModel::$OD_PR, NULL);
            $model->process($all);
            $simple->setUpdateAgent($all->dbOrder->id);
        }

        $timeEnd = time() - $timeStart;
        print $countError.' ошибок';
        print '<br>';
        print $timeEnd.' s';
    }
    public function toTrackComponent(RussianPostTracking $trackPr, TrackFull $full){
        $steps = array();

        if(true){
            $data = &$trackPr->track[0];
            $head = new TrackHeadComponent;
            $head->code     = $full->agent;
            $head->weight   = $data->weight;
            $head->np       = $data->np;
            $head->oc       = $data->oc;
            $head->address  = $data->first_address;
            $head->zip      = $data->first_zip;
        }
        foreach($trackPr->track as $value){
            $step = new StepComponent;

            $step->date         = strtotime($value->date);
            $step->zip          = $value->zip;
            $step->address      = $value->address;
            $step->operation    = mb_strtolower($value->operation, $this->encoding);
            $step->status       = mb_strtolower($value->status, $this->encoding);
            $full->step[] = clone $step;
        }
        $head->done = $this->setDone($value->status);

        $full->head = clone $head;
        return $full;
    }
    public function setDone($status){
        $done = 0;
        if($status === 'вручение адресату' OR $status === 'вручение отправителю'){
            $done = 1;
        }
        return $done;
    }
}
class TrackPRException extends CException{ public $message = 'Закончились номера';}