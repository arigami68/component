<?php

$russianpostRequiredExtensions = array('SimpleXML', 'curl', 'pcre');
foreach($russianpostRequiredExtensions as $russianpostExt) {
  if (!extension_loaded($russianpostExt)) {
    throw new RussianPostSystemException('Требуемое расширение ' . $russianpostExt . ' потеряно');
  }
}

class RussianPostAPI {
    const SOAPEndpoint = 'http://voh.russianpost.ru:8080/niips-operationhistory-web/OperationHistory';


    private static $instance;
    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {}

    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}


    public function getOperationHistory($trackingNumber) {

        $trackingNumber = trim($trackingNumber);
        if (!preg_match('/^[0-9]{14}|[A-Z]{2}[0-9]{9}[A-Z]{2}$/', $trackingNumber)) {
          throw new RussianPostArgumentException('Не правильный формат трекинга: ' . $trackingNumber);
        }
        $data = $this->makeRequest($trackingNumber);
        $data = $this->parseResponse($data, $trackingNumber);

        return $data;
    }

    protected function parseResponse($raw, $trackingNumber){
        $xml = simplexml_load_string($raw);
        if (!is_object($xml))
          throw new RussianPostDataXMLException("Не удалось разобрать ответ XML");

        $ns = $xml->getNamespaces(true);

        if(isset($ns['ns1'])) $nsTo = $ns['ns1'];
        if(isset($ns['ns2'])) $nsTo = $ns['ns2'];
        if(isset($ns['ns3'])) $nsTo = $ns['ns3'];
        if(isset($ns['ns4'])) $nsTo = $ns['ns4'];

        if(isset($ns['OperationHistoryFaultReason'])) throw new RussianPostChannelException('Сервер не отвечает');
        if (!(
                     $xml->children($ns['S'])->Body &&
          $records = $xml->children($ns['S'])->Body->children($nsTo)->OperationHistoryData->historyRecord
        ))

          throw new RussianPostDataException("Нет трекинга");

        $out = array();
        $track = new RussianPostTracking;
        $track->code = $trackingNumber;
        foreach($records as $rec) {
          $outRecord = new RussianPostTrackingRecord();
          $outRecord->operation            = (string) $rec->OperationParameters->OperType->Name;
          $outRecord->status       = (string) $rec->OperationParameters->OperAttr->Name;
          $outRecord->zip = (string) $rec->AddressParameters->OperationAddress->Index;
          $outRecord->address       = (string) $rec->AddressParameters->OperationAddress->Description;
          $outRecord->first_zip                      = (string) $rec->AddressParameters->DestinationAddress->Index;
          $outRecord->first_address                  = (string) $rec->AddressParameters->DestinationAddress->Description;
          $outRecord->date            = (string) $rec->OperationParameters->OperDate;
          $outRecord->weight               = round(floatval($rec->ItemParameters->Mass) / 1000, 3);
          $outRecord->oc            = round($rec->FinanceParameters->Value);#round(floatval($rec->FinanceParameters->Value) / 100, 2);
          $outRecord->np   = round($rec->FinanceParameters->Payment);#round(floatval($rec->FinanceParameters->Payment) / 100, 2);

          $track->track[] = clone $outRecord;
        }
        return $track;
    }

    protected function makeRequest($trackingNumber) {
        $channel = curl_init(self::SOAPEndpoint);

        $data = <<<EOD
        <s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
        <s:Header/>
        <s:Body xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
           <OperationHistoryRequest xmlns="http://russianpost.org/operationhistory/data">
               <Barcode>$trackingNumber</Barcode>
               <MessageType>0</MessageType>
           </OperationHistoryRequest>
        </s:Body>
        </s:Envelope>
EOD;
        curl_setopt_array($channel, array(
          CURLOPT_POST           => true,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_CONNECTTIMEOUT => 10,
          CURLOPT_TIMEOUT        => 10,
          CURLOPT_POSTFIELDS     => $data,
          CURLOPT_HTTPHEADER     => array(
            'Content-Type: text/xml; charset=utf-8',
            'SOAPAction: ""',
          ),
        ));

        $result = curl_exec($channel);

        if ($errorCode = curl_errno($channel)) {
          throw new RussianPostChannelException(curl_error($channel), $errorCode);
        }

        return $result;
    }
}
class RussianPostTracking{
    public $code;
    public $track;
}
class RussianPostTrackingRecord{
    public $operation;
    public $status;
    public $zip;
    public $address;
    public $date;
    public $weight;
    public $oc;
    public $np;
    public $first_address;
}

class RussianPostArgumentException  extends CException{ public $code = 1001;}
class RussianPostDataException      extends CException{ public $code = 1002;}
class RussianPostDataXMLException   extends CException{ public $code = 1003;}
class RussianPostChannelException   extends CException{ public $code = 1004;}