<?php
/*
 * @name :: SimpleBot
 * @comment :: Простой бот. Установлен в log под названием trackorder
 */
class SimpleBotSettings{
    public function __construct($array){ $this->table = $array['table'];}
    public static $lock_ON  = 1;
    public static $lock_OFF = 0;
    public $count           = 300; # Сколько значений забираем
    private $table;
    public function getTable(){ return $this->table;}
}