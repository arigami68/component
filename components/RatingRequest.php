<?php
/**
 * Class RatingRequest
 */
class RatingRequest extends RequestAbstract{
	public function init(){
		$json = $this->getJson();

		foreach($json->array as $k=>$value){

			$id 	= (int)$value->id;
			$rating = (int)$value->width/30;
			$text 	= htmlspecialchars($value->text);

			if(!($question = DBratingquestion::model()->id($id)->find())) return FALSE;

			$array[] = array(
				'id'        => $id,
				'rating'    => $rating,
				'text'      => $text,
				'user'      => Yii::app()->user->auth->contract,
				'type'      => $question->answer,
			);
		}
		foreach($array as $value){
			$this->add($value);
		}
		return true;
	}
	public function add($array){
		$record = $this->getRecord($array);

		$record->question   = $array['id'];
		$record->contract   = $array['user'];
		if($array['type'] == 0){

		}elseif($array['type'] == 1){
			$record->rating     = $array['rating'];
		}elseif($array['type'] == 2){
			$record->text     = $array['text'];
		}
		if($record->isNewRecord) $record->dateCreate = date('Y-m-d H:i');
		return $record->save();
	}
	public function getRecord($array){
		if(true){
			$criteria = new CDbCriteria;
			$criteria->condition    = ' question = '.$array['id'];
			$criteria->order        = 'id DESC';
		}
		$record = ($v = DBrating::model()->question($array['id'])->contract()->find($criteria)) ? $v : new DBrating;
		if(MegapolisRatingWidget::getsee($criteria)) $record = new DBrating;

		return $record;
	}
}