<?php

class FullAPTIComponent extends CFormModel{
    public $APTI;
    public $contract;       # Номер договора
    public $dateshipping;   # Дата когда Изменил статус на к поступлению
    public $idapplication;  # Номер заказа
    public $status;         # Статус заказа
    public $idstorage;      # id номер в Куче
    public $number;         # Количество
    public $wovat;          # Цена без НДС
    public $wivat;          # Цена с НДС
    public $store;          # Склад
    public $contractor;     # Контрагент-поклажедатель
    public $provider;       # Поставщик котрагента
    public $internal;       # Код поставщика контагента
    public $artical;        # Артикул
    public $nomencl;        # Номенклатура
    public $unit;           # Блок
    public $ratevat;        # Ставка НДС
    public $cost;           # Стоимость заказа
    public $note;           # Комментарий
    public $tabledatecreate;# Дата добавления строки
    public $tablestatus;    # Статус добавления строки
}