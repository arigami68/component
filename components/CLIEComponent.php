<?php
# Клиент. Данные по клиенту
class CLIEComponent extends CComponent{
    public static $instance;
    
    private function __construct(){}
    private function __clone()    {}
    private function __wakeup()   {} 
    
    public static function get(){ return empty(self::$instance) ? self::$instance = new self() : self::$instance;}
    public $id;
    public $db;
    public $num; # Определить как забираются номера
    public $secretKey;
    public $contract;
    public $output;
}