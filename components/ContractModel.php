<?php
class ContractModel extends CModel{
    public static $CLIENTS_TMP = 'tmp';
    public static $CLIENTS_WORK = 'work';

    public static $OPERATION_CREATE = 'create';
    public static $OPERATION_READ   = 'read';
    public static $OPERATION_UPDATE = 'update';
    public static $OPERATION_DELETE = 'delete';

    public static $OPERATION_UPDATE_STATUS = 2; # Перевод клиента из тестовой в рабочу базу

    public $client;
    public $db;         # База данных с которой мы работаем
    public $operation;  # Операция которую мы совершаем
    public $status;     # Статус
    public $output;
    public $mail;
    public $db_prod = 'CLIE';       # Боевая база
    public $magic;
    public $dir = '/www/http/lk/production/file';

    public function setMagic(ContractModel_magic $magic){$this->magic = $magic;}
    public function setMail(MAILModel $mail){$this->mail = $mail;}
    public function setOutput(OutputModel $output){$this->output = $output;}
    public function setClient(ContractModel_Client $client){$this->client = $client;}

    public function attributeNames(){}

    public function choiceDB($client){
        if($client == self::$CLIENTS_TMP){
            $this->db = new TMP_CLIE;
        }elseif($client == self::$CLIENTS_WORK){
            $this->db = new CLIE;
        }
        return $this->db;
    }
    public function validate(){
        $this->magic = new ContractModel_magic;
        switch($this->operation){
            case (self::$OPERATION_CREATE):
                $this->client->scenario = self::$OPERATION_CREATE;
                if(!$this->client->validate()){
                    $this->output->error[] = 'not selected {email}';
                    $this->output->result = 0;
                    return FALSE;
                }
                $production = new $this->db_prod;
                $criteria = new CDbCriteria();
                $criteria->select = 'id';
                $criteria->order = 'id DESC';
                $last_id = $production::model()->find($criteria);
                $this->client->id = $last_id->id+1;

                $db = clone $this->db;
                if($db->model()->email($this->client->mail)->find()){
                    $this->output->error[] = 'Duplicate {email}';
                    $this->output->result = 0;
                    return FALSE;
                }
                break;
            case (self::$OPERATION_READ):
                break;
            case (self::$OPERATION_UPDATE):

                if($this->status == self::$OPERATION_UPDATE_STATUS){
                    $this->client->scenario = self::$OPERATION_UPDATE;
                    $this->client->db = $this->db;

                    if(!$this->client->validate()){
                        $this->output->error[] = 'not selected {id}';
                        $this->output->result = 0;
                        return FALSE;
                    }
                    $production = new $this->db_prod;
                    if($production->model()->id($this->client->id)->find()){
                        $this->output->error[] = 'Duplicate {id}';
                        $this->output->result = 0;
                        return FALSE;
                    }

                    if(!$this->magic->role = CLRO::model()->contract($this->client->user->contract)->find()){
                        $this->output->error[] = 'not selected {role}';
                        $this->output->result = 0;
                        return FALSE;
                    }
                }
                break;
            case (self::$OPERATION_DELETE):
                break;
        }
        return TRUE;
    }
    public function operation(){
        switch($this->operation){
            case (self::$OPERATION_CREATE):
                    if($db = $this->create()){
                        if(true){
                            $this->mail->to = $this->client->mail;
                            $this->mail->message = 'Пройдите регистрацию. По адресу http://lk.hydra.megapolis.loc/reg. Ваш номер договора:'.$db->id;
                            $this->mail->db =new MAIL;
                            $this->mail->db->attributes = $this->mail->attributes;
                            $this->mail->db->insert();
                            $this->output->result = 1;
                        }
                    }else{
                        $this->output->error[] = '{db} Exception';
                        $this->output->result = 0;
                    }
                break;
            case (self::$OPERATION_READ):
                break;
            case (self::$OPERATION_UPDATE):
                $this->status = self::$OPERATION_UPDATE_STATUS;
                    if($this->update()){
                        $this->output->result = 1;
                    }else{
                        $this->output->error[] = 'not selected {status} operation';
                        $this->output->result = 0;
                    }
                break;
            case (self::$OPERATION_DELETE):
                break;
            default:
                $this->output->error[] = 'not selected {operation}';
                $this->output->result = 0;
                break;
        }
    }
    public function create(){
        $db = clone $this->db;

        $db->email = $this->client->mail;

        $criteria = new CDbCriteria();
        $criteria->select = 'id';
        $criteria->order = 'id DESC';

        $last_id = $this->db->model()->find($criteria);
        $last_id = $last_id->id;
        if($last_id >= $this->client->id){

        }else{
            $db->id = $this->client->id;
        }
        if($db->insert()){
            $db->contract = $db->id;
            if($db->update()) return $db;
        }else{
            $this->output->error[] = 'Duplicate {mail}';
            $this->output->result = 0;
        }
    }
    public function read(){}
    public function update(){

        if($this->status == self::$OPERATION_UPDATE_STATUS){

            $production = new $this->db_prod;

            $production->attributes = $this->client->attributes;

            if($production->insert()){
                $this->magic->role->role = 8;
                if($this->magic->role->update()){
                    return $this->createDIR();
                }
            }
        }
    }
    public function delete(){}
    public function createDIR(){
        $path = $this->dir.DS.$this->client->id;
        if(!is_dir($path)){
            if(mkdir($path, $mode = 0755)){
                if(mkdir($path.DS.'barcode', $mode = 0755)){
                    if(mkdir($path.DS.'contracts', $mode = 0755)){
                        if(mkdir($path.DS.'documents', $mode = 0755)){
                            return TRUE;
                        }
                    }
                }
            }

        }
    }

    
}
class ContractModel_Client extends CModel{
    public $id;
    public $mail;
    public $db;   # База к которой мы подключаемся
    public $user; # Пользователь

    public function attributeNames(){
        return array(
            'mail',

        );
    }
    public function validate(){
        if($this->scenario == ContractModel::$OPERATION_CREATE){
            if(!empty($this->mail)) return TRUE;
        }elseif($this->scenario == ContractModel::$OPERATION_UPDATE){
            if(empty($this->id)) return FALSE;

            $db = clone $this->db;
            if($result = $db->model()->id($this->id)->find()){
                $this->user = $result;
                return TRUE;
            }
        }
    }
}
class ContractModel_magic extends CModel{
    public $role;
    public function setRole(CLRO $role){$this->role = $role;}
    public function attributeNames(){}
}