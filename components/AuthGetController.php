<?php

class AuthGetController extends CController
{
	public $layout='application.themes.classic.layouts.column';
    public static $ACTION_CONTRACT  = 'api/api/Contract';
    public static $ACTION_TOGETNUM  = 'api/api/TOGETNUM';
    public static $ACTION_TOGETNUM2 = 'api/api/TOGETNUM2';
	public static $ACTION_CLIENT    = 'api/api/CLIENT';
    private $ON_CONTRACT  = TRUE; # Нужно ли вводить номер договора. Проверка номера договора
	public $menu=array();

	public $breadcrumbs=array();
        public function filters()
        {
            try {
                switch(Yii::app()->urlManager->parseUrl(Yii::app()->request)){
                    case self::$ACTION_CONTRACT:
                    case self::$ACTION_TOGETNUM2:
	                case self::$ACTION_CLIENT:
                        $contract = 3143142;
                        $secret   = 'e809d89a5197076193b71a6e980f226a';
                    break;
                    case self::$ACTION_TOGETNUM:
                        if(!isset($_GET['secret'])) throw new AuthenticationException;
                        $this->ON_CONTRACT = FALSE;
                        $secret   = $_GET['secret'];

                        break;
                    default:
                            if(isset($_GET['secret'])){
                                $this->ON_CONTRACT = FALSE;
                                if(!isset($_GET['secret'])) throw new AuthenticationException;
                                $secret   = $_GET['secret'];
                            }else{
                                if(!isset($_GET['client'])) throw new AuthenticationException;
                                if(!isset($_GET['client']['contract'])) throw new AuthenticationException;
                                if(!isset($_GET['client']['secret'])) throw new AuthenticationException;
                                $contract = $_GET['client']['contract'];
                                $secret   = $_GET['client']['secret'];

                                if(!$secret) throw new AuthenticationException;
                                if(!$contract && $this->ON_CONTRACT) throw new AuthenticationException;

                            }
                }

                $client = $this->ON_CONTRACT ? CLIE::model()->contract($contract)->secret($secret)->find(): $client = CLIE::model()->secret($secret)->find();
                if(!$client) throw new AuthenticationException;

            } catch (Exception $exc) {
                $output = new OutputModel;
                $output->access = 0;
                $output->error[] = $exc->message;
                die($output->output());
            }

            $CLIE = CLIEComponent::get();
            $CLIE->id = $client->id;
            $CLIE->contract = $client->contract;
            $CLIE->db = $client;
        }
}