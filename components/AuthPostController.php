<?php
# Аутентификация, но методом POST
class AuthPostController extends Controller
{
	public $layout='application.themes.classic.layouts.column';
	public $menu=array();
	public $breadcrumbs=array();
        public function filters()
        {
            # Исключение
            if(Yii::app()->urlManager->parseUrl(Yii::app()->request) == 'load/load/toOrders'){
                return false;
            }
                $logs = new LOGS_TRMU;
                ob_start();
                print_r($_POST);
                $post = ob_get_contents();
                ob_end_clean();
                $logs->post =$post;
                $logs->ip = $_SERVER["REMOTE_ADDR"];
                $logs->insert();
                try {
                    if(!isset($_POST['client'])) throw new AuthenticationException;
                    if(!isset($_POST['client']['contract'])) throw new AuthenticationException;
                    if(!isset($_POST['client']['secret'])) throw new AuthenticationException;

                    $contract = $_POST['client']['contract'];
                    $secret   = $_POST['client']['secret'];

                    if(!$secret) throw new AuthenticationException;
                    if(!$contract) throw new AuthenticationException;

                    if(!$client = CLIE::model()->contract($contract)->secret($secret)->find())
                    {
                        throw new AuthenticationException;
                    }
                } catch (Exception $exc) {
                        $output = new OutputModel;
                        $output->access = 0;
                        $output->error[] = $exc->message;
                        die($output->output());
                }
                $CLIE = &CLIEComponent::get();
                $CLIE->id = $client->id;
                $CLIE->contract = $client->contract;
                $CLIE->secretKey = $client->secretKey;
                $CLIE->num = $client->num;
        }
}