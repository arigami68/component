<?php

#Вывод данных
class OutputModel extends CModel{
    public static $FORMAT_JSON = 'json';
    
    public function attributeNames(){}

    # Ошибки
    public $error = array();
    # Операция
    public $operation;
    # Выходные данные для чтения
    public $data = '';
    # Успешный вход
    public $access = 1;
    # Формат
    public $format = 'json';
    # Результат 0 || 1
    public $result = 0;
    
    final public function output(){
        $result = NULL;
        if($this->format == self::$FORMAT_JSON){
            $result = $this->json($this);
            $result = json_encode($result);
        }
        return $result;
    }
    public function json(OutputModel $model){
        
        $array = array();
        $array['error']= $model->error;
        $array['operation']= $model->operation;
        $array['access']= $model->access;
        $array['format']= $model->format;
        $array['result']= $model->result;
        $array['data']= $model->data;
        
        return $array;
    }
}