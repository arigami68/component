<?php

class REGOComponent extends CComponent{
    public $code;
    public $error   = array();
    public $user;
    public $format  = 'json';
    public $fromnum = 0;
    public $tonum   = 100000;
    public $currentnum;
    public $count=1;
    # {num, prefix}
    public $output;
}