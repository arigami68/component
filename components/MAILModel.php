<?php

class MAILModel extends CModel{
    public static $MESSAGE_TMP = 'tmp';

    public $db;
    public $message;
    public $to;
    public $from;
    public $subject;
    public $date_create;
    public $flag = 0;
    
    public function attributeNames(){
        return array(
            'message',
            'to',
            'from',
            'subject',
            'date_create',
            'flag',
            );
    }
    public function __construct($flag){
        $this->date_create = date('Y-m-d H:i:s');
        if($flag ==  MAILModel::$MESSAGE_TMP){
            $this->from        = 'info@megapolis-exp.ru';
            $this->subject     = 'Пройдите дальнейшую регистрацию!';
        }   
        return $this;
    }
}