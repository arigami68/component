<?php
/**
 * Class abstractCSVRead
 * @comment Работа с CSV
 *
 */
abstract class abstractCSVRead{
	public $csv;
	public function setCsv($array){
		$file = $array['pathFile'];
		$this->csv = new SplFileObject($file);
		$this->csv->setCsvControl(';');
	}
	public function read($csv){
		$string = null;
		if(!$csv->eof()) {
			$string = $csv->fgetcsv();
		}

		return $string;
	}
}