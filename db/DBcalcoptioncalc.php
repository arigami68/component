<?php

class DBcalcoptioncalc extends CActiveRecord{
    private static $tableName = 'calcoptioncalc';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function clientId($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'clientId= '.$id,
        ));
        return $this;
    }
}