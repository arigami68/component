<?php

class TRMU extends CActiveRecord{
    public $code;       # Код
    public $weight;     # Ширина
    public $oc;         # ОЦ
    public $np;         # НП
    public $address;    # Адресс
    public $zip;        # Индекс
    public $status;     # Статус
    public $operation;  # Операция
    public $date;       # Дата и время операции
    public $placeOp;    # Место операции
    public $id_orders;  # id в orders
    public $user;
    private static $tableName = 'trace_megapolis_users';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function rules() {return array(array('user, code, weight, oc, np, address, zip, status, operation, date, placeOp, id_orders', 'safe'));}
    public function code($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'code = "'.$id.'"',
        ));

        return $this;
    }
}