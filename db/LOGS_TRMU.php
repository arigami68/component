<?php

class LOGS_TRMU extends CActiveRecord{

    private static $tableName = 'trace_megapolis_users';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function getDbConnection(){return Yii::app()->logs;}
}