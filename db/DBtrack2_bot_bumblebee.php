<?php

class DBtrack2_bot_bumblebee extends CActiveRecord{
    private static $tableName = 'track2_bot_bumblebee';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function status($id){
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'status= '.$id,
        ));
        return $this;
    }
    public function limited($param, $id){
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'limited '.$param.' "'.$id.'"',
        ));
        return $this;
    }
}