<?php

class DB2settingsdeliverystatus extends CActiveRecord{
    private static $tableName = 'settingsdeliverystatus';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function getDbConnection(){return Yii::app()->agent;}
}