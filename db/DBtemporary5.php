<?php

class DBtemporary5 extends CActiveRecord{
    private static $tableName = 'temporary5';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations()
    {
        return array(
            'track2'=>array(self::BELONGS_TO, 'TRA2', 'code'),
        );
    }
}