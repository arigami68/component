<?php

class DB3query extends CActiveRecord{
    public static $QUERY_SUCCESS    = 1;
    public static $QUERY_ERROR      = 2;
    private static $tableName = 'query';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function getDbConnection(){return Yii::app()->logs;}
}