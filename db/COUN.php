<?php

class COUN extends CActiveRecord{

    private static $tableName = 'ordersextra';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function mhash($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'mhash = '.$id,
        ));
        return $this;
    }
    public function rhash($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'rhash = '.$id,
        ));
        return $this;
    }
}