<?php

class TO1C extends CActiveRecord{
    public static $STATUS_1 = array('status' => 'прибыло в город назначения', 'operation' => 'обработка');
    public static $STATUS_2 = array('status' => 'вручено адресату', 'operation' => 'вручение');
    public static $STATUS_3 = array('status' => 'возврат', 'operation' => 'возврат');
    public static $STATUS_4 = array('status' => 'возврат', 'operation' => 'возврат');
    public static $STATUS_5 = array('status' => 'самовывоз', 'operation' => 'вручение');
    public static $STATUS_6 = array('status' => 'на уточнении', 'operation' => 'вручение');
    public static $STATUS_7 = array('status' => 'перенос доставки', 'operation' => 'вручение');
    public static $STATUS_8 = array('status' => 'платное хранение', 'operation' => 'на уточнении');

    public static $TRACK_SUCCESSFULLY = 1;
    public static $TRACK_NOT_FOUND = 2;
    public static $TRACK_EMPTY = 0;

    private static $tableName = 'track_of_1c';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
}