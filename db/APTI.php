<?php

class APTI extends CActiveRecord{
    public static $APP_CREATE = 0;
    public static $APP_TO_ENTER = 1;
    public static $APP_DELETE = 3;
    public static $APP_ENTERED_OF_1C = 9;
    public static $APP_ENTERED_OF_1C_full = 4;
    public static $APP_ENTERED_OF_1C_part = 8;
    
    private static $tableName = 'app_time';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations()
    {
        return array(
            'APH1'=>array(self::HAS_MANY, 'APH1', array('id_idapplication' => 'id_idapplication')),
            'AHDB'=>array(self::HAS_MANY, 'AHDB', array('idapplication' => 'id_idapplication')),
        );
    }
    public static function date_delivery($data){
        $id_app = $data['id'];
        
        $criteria = new CDbCriteria();
        $criteria->compare('id_idapplication', $id_app);
        
        $APTI = APTI::model()->user()->find($criteria);
        return $APTI->status ? date('Y-m-d', strtotime($APTI->date_shipping)) : '';
    }
    public function user($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'id_clients = '.$id,
        ));
        return $this;
    }
    public function status($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'status = '.$id,
        ));
        return $this;
    }
    public function scopes()
    {
        return array(
             'status_ap1c'=>array(
                'condition'=> 'status = '.self::$APP_ENTERED_OF_1C,
            ),
        );
    }
}