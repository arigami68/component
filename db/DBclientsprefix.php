<?php

class DBclientsprefix extends CActiveRecord{
	private static $tableName = 'clientsprefix';
	public static function model($className= __CLASS__){return parent::model($className);}
	public function tableName(){return self::$tableName;}
	public function id($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'clients_id="'.$id.'"',
		));
		return $this;
	}
}