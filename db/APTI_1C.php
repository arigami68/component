<?php

class APTI_1C extends APTI{
    
    private static $tableName = 'fcapp';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function rules()
    {
        $array = array();$a=0;
        foreach($this->attributes as $k=>$v){
            if($v['id']) continue;
            
            $array[$a++] = $k;
        }
        
        return array( array(implode(",", $array), 'safe'));
    }
    public function getDbConnection(){
        return Yii::app()->db;
    }
}