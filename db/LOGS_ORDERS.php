<?php

class LOGS_ORDERS extends CActiveRecord{
    private static $tableName = 'orders_log';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function getDbConnection(){return Yii::app()->logs;}
}