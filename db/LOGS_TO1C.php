<?php

class LOGS_TO1C extends CActiveRecord{
    private static $tableName = 'track_log_1c';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function getDbConnection(){return Yii::app()->logs;}
}