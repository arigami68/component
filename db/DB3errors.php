<?php

class DB3errors extends CActiveRecord{
    private static $tableName = 'errors';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function getDbConnection(){return Yii::app()->logs;}
}