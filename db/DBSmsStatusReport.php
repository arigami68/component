<?php

class DBSmsStatusReport extends CActiveRecord{
	private static $tableName = 'smsstatusreport';
	public static function model($className= __CLASS__){return parent::model($className);}
	public function tableName(){return self::$tableName;}
	public function report(){

		$criteria 	= new CDbCriteria();
		$pagination = new CPagination();
		$sort	 	= new CSort();

		$criteria->scopes = array('contract');
		$pagination->pageVar = 'page';
		$pagination->pageSize = 10;
		$sort->sortVar = 'sort';

		return new CActiveDataProvider(get_class($this), array(
			'criteria' => $criteria,
			'pagination'=> $pagination,
			'sort' => $sort,
		));
	}
	public function contract($id){
		$this->getDbCriteria()->mergeWith(
			array('condition'=> 'contract = "'.$id.'"')
		);

		return $this;
	}
	public function unique($agent, $phone, $num){
		$this->getDbCriteria()->mergeWith(
			array('condition'=> 'agent = "'.$agent.'" AND phone = "'.$phone.'" AND num = "'.$num.'"')
		);

		return $this;
	}
	public function scopes(){
		return array(
			'contract'=>array(
				'condition'=> 'contract = "'.Yii::app()->user->auth->contract.'"',
			),
		);
	}
}