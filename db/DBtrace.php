<?php

class DBtrace extends CActiveRecord{
	private static $tableName = 'trace';
	public static function model($className= __CLASS__){return parent::model($className);}
	public function tableName(){return self::$tableName;}

	public function id($id){$this->getDbCriteria()->mergeWith(array('condition'=> 'id = '.$id,));return $this;}
	public function code($id){$this->getDbCriteria()->mergeWith(array('condition'=> 'code = "'.$id.'"',));return $this;}
	public function idOrder($id){$this->getDbCriteria()->mergeWith(array('condition'=> 'idOrder = '.$id,));return $this;}

	public function last(){
		$this->getDbCriteria()->mergeWith(
			array(
				'order'=> 'id desc',)
		);
		return $this;
	}

	public static function setCriteriaCurrentDate($criteria, $date = NULL){
		$date = $date ? $date : date('Y-m-d');
		$criteria->mergeWith(
			array('condition'=> "dateRecord > ".$date)
		);
	}

	public function scopes()
	{
		return array(
			'status_ap1c'=>array(
				'condition'=> 'status = '.self::$APP_ENTERED_OF_1C,
			),
		);
	}
	public function relations()
	{
		return array(
			'handbookallstatus'=>array(self::HAS_ONE, 'DBhandbookAllStatus', array('status' => 'status')),
		);
	}
}