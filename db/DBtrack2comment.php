<?php

class DBtrack2comment extends CActiveRecord{
    private static $tableName = 'track2comment';
    public $od;
    public static function model($className= __CLASS__){return parent::model($className);}

    public function tableName(){return self::$tableName;}
    public function rules()
    {
        return array(
            array('od', 'safe'),
        );
    }
    public function attributeLabels()
    {
        return array(
            'od' => 'od',
        );
    }
    public function status($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'status = '.$id,
        ));
        return $this;
    }

    public function search(){
        if(isset($_GET['od'])){
            if($_GET['od'][0]){
                $this->od = $_GET['od'][0];
            }
        }
        $criteria = new CDbCriteria();
        $sort = new CSort();
        $pagination = new CPagination();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 100;
        $criteria->order = 'id DESC';
        $criteria->compare('od', $this->od,true);

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    public function getForFilter(){
        $default = isset($_GET['od']) ? $_GET['od'] : '';
        $addfilter = function($value1, $value2){
            array_unshift($value1, $value2);
            return $value1;
        };
        return CHtml::dropDownList(
            'od',
            $default,
            $addfilter(DefaultComponent::get()->HandBook(DefaultComponent::$HANDBOOK_OD), '')
        );
    }
	public function relations()
	{
		return array(
			'valueod'		=> array(self::HAS_ONE, 'DBhandbookOd', array('id' => 'od')),
			'valuestatus'	=> array(self::HAS_ONE, 'DBhandbookAllStatus', array('id' => 'status')),
		);
	}
}