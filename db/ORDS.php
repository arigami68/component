<?php

class ORDS extends CActiveRecord{
    public $registry, $orderN, $nhash, $megapolis, $agent, $ahash, $mhash, $done, $opInventory, $opCategory, $opType, $opNotification, $opFragile, $opPacking, $opTake, $dateUpdate, $dateCreate, $dateSend, $dateSurrender, $intercom, $description, $addressdep, $idClient, $clientContract, $chash, $zip, $region, $area, $city, $place, $street, $house, $building, $flat, $office, $floor, $address, $idPerson, $phone, $email, $fio, $weightEnd, $weightArticle, $countArticle, $capacity, $costDelivery, $costInsurance, $costPublic, $costArticle, $comment, $price, $idAgent, $status, $id1s, $country, $od;
    public static $STATUS_CREATE = 0;
    public static $STATUS_UPDATE = 2;
    public static $STATUS_DELETE = 4;
    
    public static $SCENARIO_DELETE = 'api_delete';
    
    private static $tableName = 'orders';
    //private static $tableName = 'orders2';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function update($attributes = NULL){
        if($this->scenario != self::$SCENARIO_DELETE){
            $this->id1s = self::$STATUS_UPDATE;
        }
        return parent::update();
    }
    public function beforeFind(){
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> ' (id1s IS NULL) OR (id1s <> '.self::$STATUS_DELETE.')',
        ));
        return $this;
    }
    public function delete(){
        if(true){
            $this->id1s = self::$STATUS_DELETE;
            $this->scenario = self::$SCENARIO_DELETE;
            return $this->update();
        }
        parent::delete();
    }
    public function rules()
    {
        return array(
            array('registry, orderN, nhash, megapolis, agent, ahash, mhash, done, opInventory, opCategory, opType, opNotification, opFragile, opPacking, opTake, dateUpdate, dateSend, dateSurrender, intercom, description, addressdep, idClient, clientContract, chash, zip, region, area, city, place, street, house, building, flat, office, floor, address, idPerson, phone, email, fio, weightEnd, weightArticle, countArticle, capacity, costDelivery, costInsurance, costPublic, costArticle, comment, price, idAgent, status, id1s, country, od', 'safe'),
        );
    }
    public function megapolis($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'megapolis = "'.$id.'"',
        ));

        return $this;
    }
    public function agent($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'agent = "'.$id.'"',
        ));

        return $this;
    }
    public function user($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'idClient = '.$id,
        ));
        return $this;
    }
    public function registry($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'registry = "'.$id.'"',
        ));
        return $this;
    }
    public function od($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'od = "'.$id.'"',
        ));
        return $this;
    }
    public function id($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'id = '.$id,
        ));
        return $this;
    }
    public function relations()
    {
        return array(
            'track'=>array(self::BELONGS_TO, 'TRA2', 'id'),
            'unique'=>array(self::BELONGS_TO, 'COUN', array('id' => 'orders')),
        );
    }
}