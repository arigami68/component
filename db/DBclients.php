<?php

class DBclients extends CActiveRecord{
    public $id;
    public $email;
    public $secretKey;
    public $name;
    public $contract;
    public $prefix;
    public $oc;
    public $np;

    private static $tableName = 'clients';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function rules()
    {
        $array = array();$a=0;
        foreach($this->attributes as $k=>$v){
            $array[$a++] = $k;
        }
        return array(array(implode(",", $array), 'safe'));
    }
    public function email($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'email="'.$id.'"',
        ));
        return $this;
    }
    public function secret($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'secretKey="'.$id.'"',
        ));
        return $this;
    }
    public function id($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'id="'.$id.'"',
        ));
        return $this;
    }
    public function contract($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'contract="'.$id.'"',
        ));
        return $this;
    }
    public function relations()
    {
        return array(
            'CONT'=>array(self::HAS_ONE, 'CONT', 'userid'),
            'DBcalcoptioncalc'=>array(self::HAS_MANY, 'DBcalcoptioncalc', array('clientId' => 'id')),
        );
    }
}