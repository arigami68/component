<?php

class DB3trackorderresidue extends DB3trackorder{
    private static $tableName = 'trackorderresidue';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
	public function idOrder($id){
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'idOrder = '.$id,
		));
		return $this;
	}
}