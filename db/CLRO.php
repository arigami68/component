<?php

class CLRO extends CActiveRecord{
    public static $ROLE_EMPTY = 0;
    
    private static $tableName = 'clients_role';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function contract($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'contract="'.$id.'"',
        ));
        return $this;
    }
}