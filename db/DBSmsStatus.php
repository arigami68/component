<?php

class DBSmsStatus extends CActiveRecord{
	private static $tableName = 'smsstatus';
	public static function model($className= __CLASS__){return parent::model($className);}
	public function tableName(){return self::$tableName;}

	public function id($id){$this->getDbCriteria()->mergeWith(array('condition'=> 'id = "'.$id.'"',));return $this;}
	public function od($id){$this->getDbCriteria()->mergeWith(array('condition'=> 'od = "'.$id.'"',));return $this;}
	public function status($id){$this->getDbCriteria()->mergeWith(array('condition'=> 'status = "'.$id.'"',));return $this;}
	public function number($id){$this->getDbCriteria()->mergeWith(array('condition'=> 'number = "'.$id.'"',));return $this;}
	public function clientID($id){$this->getDbCriteria()->mergeWith(array('condition'=> 'clientID = "'.$id.'"',));return $this;}

	public function scopes(){
		return array(
			'clientID'=>array(
				'condition'=> 'clientID = "'.Yii::app()->user->auth->id.'"',
			),
		);
	}
	public function processTemplate($array){

	}
	/**
	 * Конвертируем время в HH:MM
	 * @param $data get array(timeFrom,timeTo)
	 * @return array
	 */
	public static function getConvertTime($data){

		$fromHH = floor($data['timeFrom']/3600);
		$fromMM = ($data['timeFrom']/60)%60;
		$toHH 	= floor($data['timeTo']/3600);
		$toMM 	= ($data['timeTo']/60)%60;

		$array = array(
			'fromHH' => $fromHH,
			'fromMM' => $fromMM,
			'toHH' 	 => $toHH,
			'toMM' 	 => $toMM,
		);
		return $array;
	}
	public function relations()
	{
		return array(
			'valueod'		=> array(self::HAS_ONE, 'DBhandbookOd', array('id' => 'od')),
			'valuestatus'	=> array(self::HAS_ONE, 'DBhandbookAllStatus', array('id' => 'status')),
		);
	}
}