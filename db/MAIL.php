<?php
# extension email
class Mail extends CActiveRecord{
    public static $MAIL_SEND = 0;
    public static $MAIL_COMPLETED = 1;
    
    public static $tableName = 'mail';
    public $to, $from, $subject, $message, $date_create, $date_sender, $flag, $comment;

    
    public static function model($className= __CLASS__) {return parent::model($className);}
    public function tableName() {return self::$tableName;}
    public function rules()
    {
        return array(
            array('to, from, subject, message, date_create, date_sender, flag, comment', 'safe'),
        );
    }
    public function sender(){
        $this->dbCriteria->addCondition('flag = '.self::$MAIL_SEND);
        $flag = TRUE;
        foreach($this->findAll() as $k=>$arr)
        {
            
            $email = Yii::app()->email;
            
            foreach($email as $title=>$value){
                
                if(isset($arr->$title)){
                    $email->$title = $arr->$title;
                }
            }
            $email->send();
            
            $arr->flag = MAIL::$MAIL_COMPLETED;
            $arr->date_sender = date('Y-m-d H:i');
            
            if(!$arr->update()) $flag = FALSE;
        }
        return $flag;
    }
}