<?php

class DBratingquestion extends CActiveRecord{
    private static $tableName = 'ratingquestion';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function id($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'id = "'.$id.'"',
        ));
        return $this;
    }
	public function ident($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'ident = '.$id,
		));
		return $this;
	}
    public function scopes()
    {
        return array(
            'work'=>array(
                'condition'=> 'work = 1',
            ),
        );
    }
}