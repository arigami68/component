<?php

class AHDB extends CActiveRecord{
   
    private static $tableName = 'app_history';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations()
    {
        return array(
            'ASDB'=>array(self::HAS_ONE, 'ASDB', array('id' => 'idstorage')),
        );
    }
}