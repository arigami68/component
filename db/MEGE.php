<?php

class MEGE extends CActiveRecord{
    public static $SPLIT = ';';
    private static $tableName = 'megapolis_exp';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public static function FI(){
        $array = array('Москва');
        return $array;
    }
    
    public function search(){
        $criteria = new CDbCriteria();
        $criteria->scopes = array('user');
        $pagination = new CPagination();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        
        $sort = new CSort();
        $sort->sortVar = 'sort';
        
        return new CActiveDataProvider(get_class($this), array(
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    
    

}