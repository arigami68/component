<?php

class CLOD extends CActiveRecord{
    public static $OD_EMS        = 1;
    public static $OD_PR         = 2;
    public static $OD_DPD        = 3;
    public static $OD_IDEA       = 4;
    public static $OD_PRBanderol = 1005;
    public static $OD_IDEAPvz    = 1006;

    private static $tableName = 'calc_od';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function od($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'od = "'.$id.'"',
        ));
        return $this;
    }
}