<?php

class TRAC extends CActiveRecord{
   
    private static $tableName = 'trace';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function id($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'id = '.$id,
        ));
        return $this;
    }
}