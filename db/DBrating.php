<?php

class DBrating extends CActiveRecord{
    private static $tableName = 'rating';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
	public static function getSeeCriteria(){
		$criteria = new CDbCriteria;
		$criteria->limit = 1;
		$criteria->order = 'id DESC';
		return $criteria;
	}
    public function question($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'question = "'.$id.'"',
        ));
        return $this;
    }
	public function agent($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'agent = "'.$id.'"',
		));
		return $this;
	}

	public function contract($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'contract = "'.$id.'"',
		));
		return $this;
	}
}