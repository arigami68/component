<?php

class PRHS extends CActiveRecord{
    private static $tableName = 'prod_history';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations()
    {
        return array(
            'PRST'=>array(self::HAS_ONE, 'PRST', array('id' => 'idstorage')),
        );
    }
}