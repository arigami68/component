<?php
    
class RCDB extends CActiveRecord{
    public static $RC_STATUS_REQUEST_DELETE = 3;
    public static $RC_STATUS_REQUEST_MODIFIED = 2;
    public static $RC_STATUS_REQUEST_OPEN = 0;
    
    private static $tableName = 'request_carrier';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function rules()
    {
        return array(
            array('date_app, time_start, time_end, address_of_load, contact_name, phone, should_be_skipped, note, occupancy', 'safe'),
        );
    }
    # Возможность редактировать заявки
    # @todo - узнать откуда брать цифру в 5-ть кубов

    public function beforeSave() {
        if(parent::beforeSave() === false) return false; # Ошибка
        
        if(isset($this->status_request)){
            return self::oppMode($this);
        }else{
            return false;
        }
        return true;
    }
    
    public static function oppMode(RCDB $object){
        $time = time();
        $date_xxx = strtotime(date('Y-m-d', strtotime($object->time_start)));
        
        if($object->status_request === RCDB::$RC_STATUS_REQUEST_OPEN){
            
            if($object->occupancy >= 5){
                
                if($time > ($date_xxx + 3600*9 - 2*24*3600)){
                    return false;
                }
            }else{
                if($time > ($date_xxx + 3600*15)){
                    return false;
                }
            }
            
        }elseif($object->status_request === RCDB::$RC_STATUS_REQUEST_DELETE){
            
            if($object->occupancy >= 5){
                if($time > ($date_xxx + 3600*9 - 24*3600)){
                    return false;
                }
            }else{
                if($time > ($date_xxx + 3600*9)){
                    return false;
                }
            }
        }elseif($object->status_request === RCDB::$RC_STATUS_REQUEST_MODIFIED){
            if($object->occupancy >= 5){
                if($time > ($date_xxx + 3600*9 - 24*3600)){
                    return false;
                }
            }else{
                if($time > ($date_xxx + 3600*9)){
                    return false;
                }
            }
        }else{
            return false;
        }
        return true;
    }
}