<?php

class PRTI extends CActiveRecord{
    public static $PROD_TO_ENTER = 1;
    public static $PROD_ENTERED_OF_1C = 9;
    private static $tableName = 'prod_time';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations()
    {
        return array(
            'PRHS'=>array(self::HAS_MANY, 'PRHS', array('idapplication' => 'id_idapplication')),
        );
    }
    public function status($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'status = '.$id,
        ));
        return $this;
    }
    public function user($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'id_clients = '.$id,
        ));
        return $this;
    }
}