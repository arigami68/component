<?php

class DB3trackorder extends CActiveRecord{
    public static $lockFlag_ON = 1;
    public static $lockFlag_OFF = 0;

    private static $tableName = 'trackorder';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function getDbConnection(){return Yii::app()->logs;}
    public function lockFlag($id){
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'lockFlag = "'.$id.'"',
        ));
        return $this;
    }
    public function idOrder($id){
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'idOrder = '.$id,
        ));
        return $this;
    }
}