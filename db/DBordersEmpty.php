<?php
class DBordersEmpty extends CActiveRecord{
	private static $tableName = 'orders';
	public static function model($className= __CLASS__){return parent::model($className);}
	public function tableName(){return self::$tableName;}
	public function user($id)
	{
		$this->getDbCriteria()->mergeWith(array(
			'condition'=> 'idClient = '.$id,
		));
		return $this;
	}
}