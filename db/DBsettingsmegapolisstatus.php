<?php
/*
 *	перенесено в справочник | проверить
 */
class DBsettingsmegapolisstatus extends CActiveRecord{
    private static $tableName = 'settingsmegapolisstatus';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function status($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'status = "'.$id.'"',
        ));
        return $this;
    }
}