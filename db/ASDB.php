<?php

class ASDB extends CActiveRecord{
   
    private static $tableName = 'app_storage';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function relations()
    {
        return array(
            'idstorage'=>array(self::HAS_MANY, 'AHDB', 'idstorage'),
        );
    }
    public function search_getiw(){
        $array = array();
        $criteria = new CDbCriteria;
        
        $criteria->compare('user', Yii::app()->user->auth->id);
        
        $criteria->select ='id, dateupdate, provider';
        $all = $this->model()->findAll($criteria);
        
        foreach($all as $cell){
            $array+=$this->getApps($cell);
        }   
        
        $arr = array_map(function($array){
            
            foreach($array as $k=>$v){
                $storage = (string)$v['storage'];
                
                $sum = array(
                        'id' => $v['id'],
                        'amount' => $v['number'],
                        'date' => date('Y-m-d H:i', $v['date']),
                        'provider' => $v['provider'],
                    );
            }
            return $sum;
        }, $array);
        
        $pagination = new CPagination();
        $pagination->pageVar = 'page';
        $pagination->pageSize = 10;
        
        $sort = new CSort();
        $sort->sortVar = 'sort';
        
        return new CArrayDataProvider($arr, array(
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
        
        print_r($ar->getData());
        
        exit;
        
        return new CActiveDataProvider(get_class($this), array(
            'criteria'=>$criteria,
            'pagination'=> $pagination,
            'sort' => $sort,
        ));
    }
    
    public static function view_operation(){
        $img = '/megapolis_publish_r.png';
        $array = array(
            'read' => array(
                'src' =>  CHtml::image(Yii::getPathOfAlias('link_images').$img),
                'htmloption' =>  array('title' => 'Просмотр заявки'),
            ),
        );
        return (
                CHtml::link($array['read']['src'], '', $array['read']['htmloption'])
        );
    }
    public function getApps(ASDB $cell){
        $array = array();
        
        foreach($cell->idstorage as $k=>$storage){
            
            $array[$storage->idapplication][] = array(
                'id' => $storage->idapplication,
                'storage' => $storage->idstorage,
                'number' => $storage->number,
                'date' => $cell->dateupdate,
                'provider' => $cell->provider,
            );  
        }
        return $array;
    }
}