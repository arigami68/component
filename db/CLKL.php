<?php

class CLKL extends CActiveRecord{
    private static $tableName = 'calc_kladrindex';
    public static function model($className= __CLASS__){return parent::model($className);}
    public function tableName(){return self::$tableName;}
    public function zip($id)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition'=> 'zip="'.$id.'"',
        ));
        return $this;
    }
    public function relations()
    {
        return array(
            'MEGE'=>array(self::HAS_ONE, 'MEGE', array('idKladr' => 'kladr')),
        );
    }
}