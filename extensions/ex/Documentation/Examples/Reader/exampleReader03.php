<?php
/** Include path **/
set_include_path(get_include_path() . PATH_SEPARATOR . '../../../Classes/');

/** PHPExcel_IOFactory */
include 'PHPExcel/IOFactory.php';

$file = $_GET['file'];
$type = $_GET['type'];

if($type == 'application/vnd.ms-excel'){
    #$inputFileType = 'Excel2003XML';
}elseif($type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
    $inputFileType = 'Excel2007';
}else{
    die('Можно загружать следующие форматы: 1.xlsx');
}
//	$inputFileType = 'Excel2007';
//	$inputFileType = 'Excel2003XML';
//	$inputFileType = 'OOCalc';
//	$inputFileType = 'SYLK';
//	$inputFileType = 'Gnumeric';
//	$inputFileType = 'CSV';

//      $inputFileName = './sampleData/1.xlsx';

$inputFileName = $file;
$objReader = PHPExcel_IOFactory::createReader($inputFileType);
$objPHPExcel = $objReader->load($inputFileName);

$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,false,false);
$text = '';
foreach($sheetData as $k=>$v){
    $text.=$v[0].';';
}
print $text;

?>